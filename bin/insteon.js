//Debug(3);
/*

    grab a powerlinc usb 2143U controller
    grab a lamp switch or a lamp dimmer
    plug all in.
    dont press any buttons
    configure the ttyusb in main function /tty/yourUSB
    build autocom
    copy binary here int his folder
    make sure you have serial and usb accesible by user
    /etc/rules.d/<>
    user memeber of dialout
    run: ./autocom ./insteon.js

*/


state <-{
    NONE=0,
    LINKING=1
    PLAYING=2,
    DATABASE=3,
}

ins <- {

    reset = [0x2,0x67],
    version = [0x2, 0x60],
    cancelink = [0x2, 0x65],
    cancelok = [0x02,0x65,0x06],
    cancelfail = [0x02,0x65,0x15],
    linkdel = [0x2,0x64,0x00,0xFF],
    linkall = [0x2,0x64,0x01,0x01], // link group 1
    startlinkok = [0x2,0x64,0x01,0x03],
    dbfirst = [0x2, 0x69],
    dbnext  = [0x2, 0x6A],
    grpon = [0x02, 0x61, 0x01, 0x11, 0x00],

    
    linkallall = 15,
    linkresp = [],
    linkdevs = [],
    devarray = [],
    curdev = 0,
    curintens = 255,

    startlink = 0,
    tout = 60000,
    state = state.NONE,
    eof = [0x02,0x60]
};

function main(xxx)
{
    //local ser = Device("tty:///home/marius/ddd,19200,8N1", "usb", BINARY);
    local ser = Device("tty:///dev/ttyUSB1,19200,8N1", "usb", BINARY);
    ser.settimeout(4000, 4);


    print("Reset db ? \n");
    local yn = Cin();
    print("got answer " + yn + "\n");
    if(yn=="y")
    {
        print("Reset database \n");
        local ret = ser.writeread(ins.reset);
        DbgArr(ret,0);
        Sleep(2000);
    }

    print("Read  version \n");
    local ret = ser.writeread(ins.version);
    DbgArr(ret,0);
    Sleep(2000);


    print("Canceling all linking \n");
    ser.writeread(ins.cancelink);


    if(yn=="y")
    {
        print("Deleting all links \n");
        ret =  ser.writeread(ins.linkdel);
        DbgArr(ret,0);
        Sleep(3000);
    }
    else
        ins.tout=1000;

    print("Discovering devices \n");
    print("=====>Press pair on first device \n");

    Looping();

    ser.write(ins.linkall);
    print("Start receiving link messages \n");
    ins.state = state.LINKING;
    return Run();
}

function loop(what, sg, data)
{
    local ret = PROCESSED;

    if(sg==BINARY)
    {
        switch(ins.state)
        {
            case state.LINKING:
                ins.linkresp.extend(data);
                print("..... Got some data ");
                print(Arr2str(ins.linkresp,' '));
                print("\n");
                if(ins.linkresp.len()>=10)
                {
                    print("=====>Press pair on next device \n");

                    print("Canceling all linking \n");
                    local ret =  Dev("usb").writeread(ins.cancelink,1000);
                    if(ArrEqual(ret, ins.cancelok)==0)
                        print("Canceled \n");
                    if(ArrEqual(ret, ins.cancelfail)==0)
                        print("Failed \n");
                    Sleep(1000);
                    DbgArr(ret,0);
                    ins.state = state.LINKING;
                    Dev("usb").write(ins.linkall);
                    ins.linkresp.clear();
                }
                break;
            case state.DATABASE:
                ins.linkdevs.extend(data);
                print("chunk :")
                DbgArr(data,0);
                print("cumul :")
                DbgArr(ins.linkdevs,0);
                if(ins.linkdevs.len()>= 3 )
                {
                    if(ins.linkdevs[0]!=0x2)
                    {
                        print("bad frame \n");
                        ins.linkdevs.clear();
                        ret = PROCESSED;
                        break;
                    }

                    if(ArrEqual([0x2,0x6A,0x15], ins.linkdevs.slice(0,3))==0)
                    {
                        print("\n----------DB END-----------\n");
                        ins.state = state.NONE;
                        ret = PROCESSED;
                        sendX10(ins.devarray);
                        break;
                    }
                    if(ins.linkdevs[1]==0x65)
                    {
                        ins.linkdevs.clear();
                        ret = PROCESSED;
                        break;
                    }
                    if(ins.linkdevs.len()>=13)
                    {
                        //          0  1  2  3  4  5  6  7  8  9
                        // 0  1  2  3  4  5  6  7  8  9  10 11 12
                        // 02 69 06 02 57 E2 01 47 14 D1 01 0E 43
                        if((ins.linkdevs[1]==0x69 ||ins.linkdevs[1]==0x6A) &&
                            ins.linkdevs[3]==0x2 &&
                            ins.linkdevs[4]==0x57)
                        {
                            local mac = [];
                            mac.append(ins.linkdevs[7]);
                            mac.append(ins.linkdevs[8]);
                            mac.append(ins.linkdevs[9]);
                            print ("Got Device with  MAC address: " + Arr2str(mac,':') + "\n");
                            print(" group: " + ins.linkdevs[6] + "\n");

                            local ctrlflag = ins.linkdevs[5];
                            if(ctrlflag & 0x6)
                                print(" IM is controller\n");
                            else
                                print(" IM is responder\n");

                            ret = PROCESSED;
                            print("==== NEXT ASKING FOR DATABASE \n");
                            Dev("usb").write(ins.dbnext);

                            control(ins.linkdevs.slice(0,12));
                            ins.linkdevs = ins.linkdevs.slice(12, ins.linkdevs.len()-1);

                        }
                        else
                        {
                            print("Invalid frame: " + Arr2str(ins.linkdevs,' '));
                            ins.linkdevs.clear();
                        }
                    }
                }
                else
                {
                    ret = CONTINUE;
                }
                break;
        }
    }
    else if(sg==IDLING)
    {
        if(data>ins.tout && ins.state == state.LINKING) //seconds
        {
            print("Canceling all linking after 60 seconds \n");
            Dev("usb").write(ins.cancelink);
            //Dev("usb").write(ins.grpon);
            print("All linked devices");
            DbgArr(ins.linkresp,0);
            ins.state = state.NONE;
            print("==== FIRST ASKING FOR DATABASE \n");

            ins.state=state.DATABASE;
            Dev("usb").write(ins.dbfirst);
        }
        else if(ins.state == state.PLAYING)
        {
            if(ins.devarray.len())
            {
                if(ins.curdev > ins.devarray.len()-1)
                {
                    ins.curdev=0;
                }
                local device = ins.devarray[ins.curdev];
                local macadr = device.slice(7,9); //SubArr2str(arr[l],7,3,':');


                local oncmd = [0x2,0x62,0x47,0x14,0xd1,15,0x11,0x00];
                oncmd[2]=device[7];
                oncmd[3]=device[8];
                oncmd[4]=device[9];
                oncmd[7]=ins.curintens;
                if(ins.curintens==0)
                    oncmd[6]=0x13;//on
                else
                    oncmd[6]=0x11;//off

                print("X10 CMD-> " + Arr2str(oncmd,' ') + "-> \n");
                Dev("usb").write(oncmd);


                ins.curdev++;
                ins.curintens-=64;
                if(ins.curintens<32)
                    ins.curintens=255;
                Sleep(5000);
            }
        }
    }
    return ret;
}

function control(links)
{
    ins.devarray.append(links);
}

function sendX10(arr)
{
    local ret;
    print("================ DB LIST ===============\n");
    for(local l = 0; l<arr.len();l++)
    {
        ret = SubArr2str(arr[l],7,3,':');
        print("=======> DB: " + ret + "\n");
    }
    ins.state=state.PLAYING;
}
