#QT += core
QT -= gui

TARGET = autocom
CONFIG += console
CONFIG -= app_bundle
QMAKE_CXXFLAGS +=  -std=c++11
TEMPLATE = app

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O0  -Wno-unused-parameter

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG += -O0  -Wno-unused-parameter
DEFINES += DEBUG

#DEFINES += SCRAT_NO_ERROR_CHECKING

INCLUDEPATH += sq/include
INCLUDEPATH += sq/squirrel
INCLUDEPATH += sq/sqrat/include
INCLUDEPATH += sq/sqrat
INCLUDEPATH += sq/sqrat/include/sqrat

INCLUDEPATH += /usr/include/libusb-1.0

SOURCES += main.cpp \
    sqwrap.cpp \
    serialcom.cpp \
    serialport.cpp \
    sqbytearr.cpp \
    divais.cpp \
    plugins.cpp \
    scrhelp.cpp \
    usb.cpp


HEADERS += \
    main.h \
    sqwrap.h \
    serialcom.h \
    icombase.h \
    osthread.h \
    serialport.h \
    icombase.h \
    sqbytearr.h \
    divais.h \
    plugins.h \
    iodata.h \
    msqqueue.h \
    usb.h



LIBS += -ldl -lpthread


DISTFILES += \
    bin/pay.sss \
    bin/Script.sss \
    bin/payutil.sss \
    bin/first.sss \
    bin/settings.sss \
    bin/test.sss \
    bin/phytest.sss \
    bin/phypay.sss \
    bin/fakeser.sss \
    bin/include.sss \
    bin/offlinetest.sss \
    bin/pay.sss \
    bin/sync.sss \
    bin/ssh.sss \
    bin/test.sss \
    bin/readsens.ss \
    bin/quanim_set0.sss \
    bin/insteon.scr \
    bin/insteon.js



target.path = ./bin
INSTALLS += target




unix:!macx: LIBS += -L$$PWD/lib/ -lsquirrel

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libsquirrel.a

unix:!macx: LIBS += -L$$PWD/lib/ -lsqstdlib

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libsqstdlib.a

unix:!macx|win32: LIBS += -lusb-1.0
