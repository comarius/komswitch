#ifndef MAIN_H_
#define MAIN_H_

#include "msqqueue.h"

class Device;
class RunCtx
{
public:
    static int Run( );
    static int Exit(int slp);
    static void XSleep(int t);
    static const char* num2str(int hex, int bytes);
    static int arr2rint(Sqrat::Array a, int from, int bytes);
    static int arr2int(Sqrat::Array a, int from, int bytes);
    static const char* arr2ascii(Sqrat::Array a);
    static const char* DbgArr(Sqrat::Array a, int);
    static const char* arr2str(Sqrat::Array a,char sep);
    static const char* parr2str(Sqrat::Array a, int,int,char sep);
    static int str2num(const char* str);
    static Sqrat::Array str2arr(const char* str);
    static Sqrat::Array num2arr(int hex, int bytes);
    static std::string  toString(cbyte* binary, int len);
    static bstring  toBinary(const char* string);
    static bstring  toBinary2(const char* string);
    static void setdebug(int deb);
    static void Delete(Device* p);
    static bool sexecute(std::vector<std::string> & prog);
    static std::string  kbhit();
    static void shellio();
    static Device* dev(const char* name);
    static void callScript(const Device* who, int err, const char* msg);
    static const char* coin();
    static bool idle();
    static const char* System(const char*);
    static int SystemCb(const char* comand, Sqrat::Function f);
    static int loopit();
    static int help();
    static void addThreadSpin(std::string& name, const char* dir, int lop);
    static int callSqFoo(const Device*, Sqrat::Function& f, const CntPtr<DevMsg>& msg);
    static int exitonidle();
    static int WaitDevices(int to);
    static int ArrFind(Sqrat::Array a, Sqrat::Array b);
    static int ArrEqual(Sqrat::Array a, Sqrat::Array b);
    static int  Error(const char* msg);
    static int Log(const char* logstr);
    static int EnableLog(const char* logfile, int max, int many);
    static int Looping();
    static int Tick();
    static void Notify(bool b){_looPing=b;} // start acum events now
    static void Plug(const char* dll);
private:
    static int _run(int interval, bool loop=true);
    static int  _LogManage();

public:
    static std::set<Device*>    _Devices;
    static bool                 _Alive;
    static bool                 _looPing;
    static SqEnv*               _Penv;
    static MsgQueue             _MsgQue;
    static bool                 _ShellOn;
    static bool                 _InScript;
    static bool                 _InCallback;
    static int                  _LoopSpin;
    static std::map<std::string, std::string> _threads;
    static Sqrat::Function      _funcloop;
    static CntPtr<DevMsg>*      _curMsg;
    static std::string          _LogFile;
    static long                 _LogFileSz;
    static int                  _LogFiles;
    static long                 _LogFileLastSz;
    static SoEntry*             _plugin;
};

extern RunCtx*  PCTX;
extern int      _Debug;
extern SqEnv*   PSq;
#endif
