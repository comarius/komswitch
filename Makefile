#############################################################################
# Makefile for building: autocom
# Generated by qmake (3.0) (Qt 5.5.1)
# Project:  autocom.pro
# Template: app
# Command: /usr/lib/x86_64-linux-gnu/qt5/bin/qmake -o Makefile autocom.pro
#############################################################################

MAKEFILE      = Makefile

####### Compiler, tools and options

CC            = gcc
CXX           = g++
DEFINES       = -DDEBUG -DQT_NO_DEBUG -DQT_CORE_LIB
CFLAGS        = -m64 -pipe -O2 -Wall -W -D_REENTRANT -fPIC $(DEFINES)
CXXFLAGS      = -m64 -pipe -std=c++11 -O0 -Wno-unused-parameter -Wall -W -D_REENTRANT -fPIC $(DEFINES)
INCPATH       = -I. -Isq/include -Isq/squirrel -Isq/sqrat/include -Isq/sqrat -Isq/sqrat/include/sqrat -isystem /usr/include/libusb-1.0 -I. -I. -isystem /usr/include/x86_64-linux-gnu/qt5 -isystem /usr/include/x86_64-linux-gnu/qt5/QtCore -I. -I/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64
QMAKE         = /usr/lib/x86_64-linux-gnu/qt5/bin/qmake
DEL_FILE      = rm -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p
COPY          = cp -f
COPY_FILE     = cp -f
COPY_DIR      = cp -f -R
INSTALL_FILE  = install -m 644 -p
INSTALL_PROGRAM = install -m 755 -p
INSTALL_DIR   = cp -f -R
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
TAR           = tar -cf
COMPRESS      = gzip -9f
DISTNAME      = autocom1.0.0
DISTDIR = /home/marius/CPP/komswitch/.tmp/autocom1.0.0
LINK          = g++
LFLAGS        = -m64 -Wl,-O1
LIBS          = $(SUBLIBS) -ldl -L/home/marius/CPP/komswitch/lib/ -lsquirrel -lsqstdlib -lusb-1.0 -lQt5Core -lpthread 
AR            = ar cqs
RANLIB        = 
SED           = sed
STRIP         = strip

####### Output directory

OBJECTS_DIR   = ./

####### Files

SOURCES       = main.cpp \
		sqwrap.cpp \
		serialcom.cpp \
		serialport.cpp \
		sqbytearr.cpp \
		divais.cpp \
		plugins.cpp \
		scrhelp.cpp \
		usb.cpp 
OBJECTS       = main.o \
		sqwrap.o \
		serialcom.o \
		serialport.o \
		sqbytearr.o \
		divais.o \
		plugins.o \
		scrhelp.o \
		usb.o
DIST          = bin/pay.sss \
		bin/Script.sss \
		bin/payutil.sss \
		bin/first.sss \
		bin/settings.sss \
		bin/test.sss \
		bin/phytest.sss \
		bin/phypay.sss \
		bin/fakeser.sss \
		bin/include.sss \
		bin/offlinetest.sss \
		bin/pay.sss \
		bin/sync.sss \
		bin/ssh.sss \
		bin/test.sss \
		bin/readsens.ss \
		bin/quanim_set0.sss \
		bin/insteon.scr \
		bin/insteon.js \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/spec_pre.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/unix.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/linux.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/sanitize.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/gcc-base.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/gcc-base-unix.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/g++-base.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/g++-unix.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/qconfig.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_bootstrap_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_concurrent.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_concurrent_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_core.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_core_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_dbus.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_dbus_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_eglfs_device_lib_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_gui.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_gui_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_network.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_network_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_opengl.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_opengl_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_openglextensions.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_openglextensions_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_platformsupport_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_printsupport.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_printsupport_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_sql.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_sql_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_testlib.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_testlib_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_widgets.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_widgets_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xcb_qpa_lib_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xml.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xml_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt_functions.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt_config.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64/qmake.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/spec_post.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/exclusive_builds.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/default_pre.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/resolve_config.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/default_post.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/warn_on.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/resources.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/moc.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/unix/thread.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/testcase_targets.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/exceptions.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/yacc.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/lex.prf \
		autocom.pro main.h \
		sqwrap.h \
		serialcom.h \
		icombase.h \
		osthread.h \
		serialport.h \
		icombase.h \
		sqbytearr.h \
		divais.h \
		plugins.h \
		iodata.h \
		msqqueue.h \
		usb.h main.cpp \
		sqwrap.cpp \
		serialcom.cpp \
		serialport.cpp \
		sqbytearr.cpp \
		divais.cpp \
		plugins.cpp \
		scrhelp.cpp \
		usb.cpp
QMAKE_TARGET  = autocom
DESTDIR       = #avoid trailing-slash linebreak
TARGET        = autocom


first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

$(TARGET): /home/marius/CPP/komswitch/lib/libsquirrel.a /home/marius/CPP/komswitch/lib/libsqstdlib.a $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

Makefile: autocom.pro /usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64/qmake.conf /usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/spec_pre.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/unix.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/linux.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/sanitize.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/gcc-base.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/gcc-base-unix.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/g++-base.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/g++-unix.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/qconfig.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_bootstrap_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_concurrent.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_concurrent_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_core.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_core_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_dbus.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_dbus_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_eglfs_device_lib_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_gui.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_gui_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_network.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_network_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_opengl.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_opengl_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_openglextensions.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_openglextensions_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_platformsupport_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_printsupport.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_printsupport_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_sql.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_sql_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_testlib.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_testlib_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_widgets.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_widgets_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xcb_qpa_lib_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xml.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xml_private.pri \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt_functions.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt_config.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64/qmake.conf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/spec_post.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/exclusive_builds.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/default_pre.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/resolve_config.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/default_post.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/warn_on.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/resources.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/moc.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/unix/thread.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/testcase_targets.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/exceptions.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/yacc.prf \
		/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/lex.prf \
		autocom.pro \
		/usr/lib/x86_64-linux-gnu/libQt5Core.prl
	$(QMAKE) -o Makefile autocom.pro
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/spec_pre.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/unix.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/linux.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/sanitize.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/gcc-base.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/gcc-base-unix.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/g++-base.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/common/g++-unix.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/qconfig.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_bootstrap_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_concurrent.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_concurrent_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_core.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_core_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_dbus.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_dbus_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_eglfs_device_lib_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_gui.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_gui_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_network.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_network_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_opengl.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_opengl_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_openglextensions.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_openglextensions_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_platformsupport_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_printsupport.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_printsupport_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_sql.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_sql_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_testlib.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_testlib_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_widgets.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_widgets_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xcb_qpa_lib_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xml.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/modules/qt_lib_xml_private.pri:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt_functions.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt_config.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64/qmake.conf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/spec_post.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/exclusive_builds.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/default_pre.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/resolve_config.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/default_post.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/warn_on.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/qt.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/resources.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/moc.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/unix/thread.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/testcase_targets.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/exceptions.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/yacc.prf:
/usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/lex.prf:
autocom.pro:
/usr/lib/x86_64-linux-gnu/libQt5Core.prl:
qmake: FORCE
	@$(QMAKE) -o Makefile autocom.pro

qmake_all: FORCE


all: Makefile $(TARGET)

dist: distdir FORCE
	(cd `dirname $(DISTDIR)` && $(TAR) $(DISTNAME).tar $(DISTNAME) && $(COMPRESS) $(DISTNAME).tar) && $(MOVE) `dirname $(DISTDIR)`/$(DISTNAME).tar.gz . && $(DEL_FILE) -r $(DISTDIR)

distdir: FORCE
	@test -d $(DISTDIR) || mkdir -p $(DISTDIR)
	$(COPY_FILE) --parents $(DIST) $(DISTDIR)/
	$(COPY_FILE) --parents main.h sqwrap.h serialcom.h icombase.h osthread.h serialport.h icombase.h sqbytearr.h divais.h plugins.h iodata.h msqqueue.h usb.h $(DISTDIR)/
	$(COPY_FILE) --parents main.cpp sqwrap.cpp serialcom.cpp serialport.cpp sqbytearr.cpp divais.cpp plugins.cpp scrhelp.cpp usb.cpp $(DISTDIR)/


clean: compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) *~ core *.core


distclean: clean 
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) Makefile


####### Sub-libraries

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

check: first

compiler_rcc_make_all:
compiler_rcc_clean:
compiler_moc_header_make_all:
compiler_moc_header_clean:
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: 

####### Compile

main.o: main.cpp main.h \
		msqqueue.h \
		sq/sqrat/include/sqrat.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h \
		sq/sqrat/include/sqrat/sqratTable.h \
		sq/sqrat/include/sqrat/sqratObject.h \
		sq/sqrat/include/sqrat/sqratAllocator.h \
		sq/sqrat/include/sqrat/sqratTypes.h \
		sq/sqrat/include/sqrat/sqratClassType.h \
		sq/sqrat/include/sqrat/sqratUtil.h \
		sq/sqrat/include/sqrat/sqratOverloadMethods.h \
		sq/include/sqstdaux.h \
		sq/sqrat/include/sqrat/sqratGlobalMethods.h \
		sq/sqrat/include/sqrat/sqratMemberMethods.h \
		sq/sqrat/include/sqrat/sqratFunction.h \
		sq/sqrat/include/sqrat/sqratClass.h \
		sq/sqrat/include/sqrat/sqratConst.h \
		sq/sqrat/include/sqrat/sqratScript.h \
		sq/include/sqstdio.h \
		sq/sqrat/include/sqrat/sqratArray.h \
		icombase.h \
		osthread.h \
		sqwrap.h \
		sq/include/sqstdblob.h \
		sq/include/sqstdmath.h \
		sq/include/sqstdsystem.h \
		sq/include/sqstdstring.h \
		plugins.h \
		divais.h \
		iodata.h \
		serialcom.h \
		serialport.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

sqwrap.o: sqwrap.cpp sqwrap.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h \
		sq/include/sqstdio.h \
		sq/include/sqstdaux.h \
		sq/include/sqstdblob.h \
		sq/include/sqstdmath.h \
		sq/include/sqstdsystem.h \
		sq/include/sqstdstring.h \
		sq/sqrat/include/sqrat.h \
		sq/sqrat/include/sqrat/sqratTable.h \
		sq/sqrat/include/sqrat/sqratObject.h \
		sq/sqrat/include/sqrat/sqratAllocator.h \
		sq/sqrat/include/sqrat/sqratTypes.h \
		sq/sqrat/include/sqrat/sqratClassType.h \
		sq/sqrat/include/sqrat/sqratUtil.h \
		sq/sqrat/include/sqrat/sqratOverloadMethods.h \
		sq/sqrat/include/sqrat/sqratGlobalMethods.h \
		sq/sqrat/include/sqrat/sqratMemberMethods.h \
		sq/sqrat/include/sqrat/sqratFunction.h \
		sq/sqrat/include/sqrat/sqratClass.h \
		sq/sqrat/include/sqrat/sqratConst.h \
		sq/sqrat/include/sqrat/sqratScript.h \
		sq/sqrat/include/sqrat/sqratArray.h \
		osthread.h \
		icombase.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o sqwrap.o sqwrap.cpp

serialcom.o: serialcom.cpp main.h \
		msqqueue.h \
		sq/sqrat/include/sqrat.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h \
		sq/sqrat/include/sqrat/sqratTable.h \
		sq/sqrat/include/sqrat/sqratObject.h \
		sq/sqrat/include/sqrat/sqratAllocator.h \
		sq/sqrat/include/sqrat/sqratTypes.h \
		sq/sqrat/include/sqrat/sqratClassType.h \
		sq/sqrat/include/sqrat/sqratUtil.h \
		sq/sqrat/include/sqrat/sqratOverloadMethods.h \
		sq/include/sqstdaux.h \
		sq/sqrat/include/sqrat/sqratGlobalMethods.h \
		sq/sqrat/include/sqrat/sqratMemberMethods.h \
		sq/sqrat/include/sqrat/sqratFunction.h \
		sq/sqrat/include/sqrat/sqratClass.h \
		sq/sqrat/include/sqrat/sqratConst.h \
		sq/sqrat/include/sqrat/sqratScript.h \
		sq/include/sqstdio.h \
		sq/sqrat/include/sqrat/sqratArray.h \
		icombase.h \
		osthread.h \
		sqwrap.h \
		sq/include/sqstdblob.h \
		sq/include/sqstdmath.h \
		sq/include/sqstdsystem.h \
		sq/include/sqstdstring.h \
		plugins.h \
		serialcom.h \
		serialport.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o serialcom.o serialcom.cpp

serialport.o: serialport.cpp icombase.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h \
		osthread.h \
		serialport.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o serialport.o serialport.cpp

sqbytearr.o: sqbytearr.cpp sqbytearr.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o sqbytearr.o sqbytearr.cpp

divais.o: divais.cpp main.h \
		msqqueue.h \
		sq/sqrat/include/sqrat.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h \
		sq/sqrat/include/sqrat/sqratTable.h \
		sq/sqrat/include/sqrat/sqratObject.h \
		sq/sqrat/include/sqrat/sqratAllocator.h \
		sq/sqrat/include/sqrat/sqratTypes.h \
		sq/sqrat/include/sqrat/sqratClassType.h \
		sq/sqrat/include/sqrat/sqratUtil.h \
		sq/sqrat/include/sqrat/sqratOverloadMethods.h \
		sq/include/sqstdaux.h \
		sq/sqrat/include/sqrat/sqratGlobalMethods.h \
		sq/sqrat/include/sqrat/sqratMemberMethods.h \
		sq/sqrat/include/sqrat/sqratFunction.h \
		sq/sqrat/include/sqrat/sqratClass.h \
		sq/sqrat/include/sqrat/sqratConst.h \
		sq/sqrat/include/sqrat/sqratScript.h \
		sq/include/sqstdio.h \
		sq/sqrat/include/sqrat/sqratArray.h \
		icombase.h \
		osthread.h \
		sqwrap.h \
		sq/include/sqstdblob.h \
		sq/include/sqstdmath.h \
		sq/include/sqstdsystem.h \
		sq/include/sqstdstring.h \
		plugins.h \
		divais.h \
		iodata.h \
		usb.h \
		serialcom.h \
		serialport.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o divais.o divais.cpp

plugins.o: plugins.cpp plugins.h \
		osthread.h \
		icombase.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o plugins.o plugins.cpp

scrhelp.o: scrhelp.cpp main.h \
		msqqueue.h \
		sq/sqrat/include/sqrat.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h \
		sq/sqrat/include/sqrat/sqratTable.h \
		sq/sqrat/include/sqrat/sqratObject.h \
		sq/sqrat/include/sqrat/sqratAllocator.h \
		sq/sqrat/include/sqrat/sqratTypes.h \
		sq/sqrat/include/sqrat/sqratClassType.h \
		sq/sqrat/include/sqrat/sqratUtil.h \
		sq/sqrat/include/sqrat/sqratOverloadMethods.h \
		sq/include/sqstdaux.h \
		sq/sqrat/include/sqrat/sqratGlobalMethods.h \
		sq/sqrat/include/sqrat/sqratMemberMethods.h \
		sq/sqrat/include/sqrat/sqratFunction.h \
		sq/sqrat/include/sqrat/sqratClass.h \
		sq/sqrat/include/sqrat/sqratConst.h \
		sq/sqrat/include/sqrat/sqratScript.h \
		sq/include/sqstdio.h \
		sq/sqrat/include/sqrat/sqratArray.h \
		icombase.h \
		osthread.h \
		sqwrap.h \
		sq/include/sqstdblob.h \
		sq/include/sqstdmath.h \
		sq/include/sqstdsystem.h \
		sq/include/sqstdstring.h \
		plugins.h \
		divais.h \
		iodata.h \
		serialcom.h \
		serialport.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o scrhelp.o scrhelp.cpp

usb.o: usb.cpp usb.h \
		icombase.h \
		sq/include/squirrel.h \
		sq/include/sqconfig.h \
		sq/sqrat/include/sqrat.h \
		sq/sqrat/include/sqrat/sqratTable.h \
		sq/sqrat/include/sqrat/sqratObject.h \
		sq/sqrat/include/sqrat/sqratAllocator.h \
		sq/sqrat/include/sqrat/sqratTypes.h \
		sq/sqrat/include/sqrat/sqratClassType.h \
		sq/sqrat/include/sqrat/sqratUtil.h \
		sq/sqrat/include/sqrat/sqratOverloadMethods.h \
		sq/include/sqstdaux.h \
		sq/sqrat/include/sqrat/sqratGlobalMethods.h \
		sq/sqrat/include/sqrat/sqratMemberMethods.h \
		sq/sqrat/include/sqrat/sqratFunction.h \
		sq/sqrat/include/sqrat/sqratClass.h \
		sq/sqrat/include/sqrat/sqratConst.h \
		sq/sqrat/include/sqrat/sqratScript.h \
		sq/include/sqstdio.h \
		sq/sqrat/include/sqrat/sqratArray.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o usb.o usb.cpp

####### Install

install_target: first FORCE
	@test -d $(INSTALL_ROOT)/home/marius/CPP/komswitch/./bin || mkdir -p $(INSTALL_ROOT)/home/marius/CPP/komswitch/./bin
	-$(INSTALL_PROGRAM) $(QMAKE_TARGET) $(INSTALL_ROOT)/home/marius/CPP/komswitch/bin/$(QMAKE_TARGET)
	-$(STRIP) $(INSTALL_ROOT)/home/marius/CPP/komswitch/bin/$(QMAKE_TARGET)

uninstall_target: FORCE
	-$(DEL_FILE) $(INSTALL_ROOT)/home/marius/CPP/komswitch/bin/$(QMAKE_TARGET)
	-$(DEL_DIR) $(INSTALL_ROOT)/home/marius/CPP/komswitch/./bin/ 


install: install_target  FORCE

uninstall: uninstall_target  FORCE

FORCE:

