

#include <unistd.h>
#include <string>
#include <deque>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "sqwrap.h"
#include "sqratConst.h"
#include "icombase.h"
#include "divais.h"
#include "serialcom.h"
#include "main.h"

#ifdef __linux
#include <set>
#include <sys/ioctl.h>
#include <termios.h>
#endif


int RunCtx::help()
{
    std::cout << TC_YELLOW << "\n";
    std::cout << "-------------------------------------------------------------------" << std::endl;
    std::cout << "|                      API's                                      |" << std::endl;
    std::cout << "-------------------------------------------------------------------" << std::endl;
    std::cout << "GLOBAL CONSTANTS" << std::endl;
    std::cout << "    Data types" << std::endl;
    std::cout << "    Message Id's, loop second parameter" << std::endl;
    std::cout << TC_YELLOW << "         CLOSED" << TC_WHITE << " Message id in loop(), A device is closed," << std::endl;
    std::cout << TC_YELLOW << "         IDLING" << TC_WHITE << " Message id in loop(), Process idling" << std::endl;
    std::cout << TC_YELLOW << "         OPENED" << TC_WHITE << " Message id in loop(), A device is opened" << std::endl;
    std::cout << TC_YELLOW << "         BINARY" << TC_WHITE << " Message id in loop(), Data is binary" << std::endl;
    std::cout << TC_YELLOW << "         TEXT" << TC_WHITE << " Message id in loop(), Data is text" << std::endl;
    std::cout << TC_YELLOW << "         RECEIVING" << TC_WHITE << " message id in loop(), Device is receiveing. For file transfer" << std::endl;
    std::cout << TC_YELLOW << "         SENDING" << TC_WHITE << " message id in loop(), Device is sending. For file transfer" << std::endl;
    std::cout << "\n    Return codes from loop" << std::endl;
    std::cout << TC_YELLOW << "         PROCESSED" << TC_WHITE << " Return it from loop() if you read/processed the data, so internal cache coul dbe deleted" << std::endl;
    std::cout << TC_YELLOW << "         CONTINUE" << TC_WHITE << " Return it from loop() to keep the script Run(), running, and received dat to be hold for as much next loop so it can be read with read/gets device methods" << std::endl;
    std::cout << TC_YELLOW << "         EXIT" << TC_WHITE << " Return it from loop() to terminate the script" << std::endl;
    std::cout << "\n    optional timeout predefined values for Sleep/wait/read/gets Api's" << std::endl;
    std::cout << TC_YELLOW << "         AYEAR" << TC_WHITE << " a year optional timeout" << std::endl;
    std::cout << TC_YELLOW << "         ADAY" << TC_WHITE << " a dey timeut" << std::endl;
    std::cout << TC_YELLOW << "         ANHOUR" << TC_WHITE << " an hour optional timeout" << std::endl;
    std::cout << TC_YELLOW << "         INFINITE" << TC_WHITE << " no optional timeout" << std::endl;
    std::cout << "\n    Predefined key codes to send with Device.sendkey() API" << std::endl;
    std::cout << TC_YELLOW << "         F1" << TC_WHITE << " key F1" << std::endl;
    std::cout << TC_YELLOW << "         UP" << TC_WHITE << " key" << std::endl;
    std::cout << TC_YELLOW << "         DOWN" << TC_WHITE << "  key" << std::endl;
    std::cout << TC_YELLOW << "         LEFT" << TC_WHITE << "" << std::endl;
    std::cout << TC_YELLOW << "         RIGHT" << TC_WHITE << "" << std::endl;
    std::cout << TC_YELLOW << "         DEL" << TC_WHITE << "" << std::endl;
    std::cout << TC_YELLOW << "         INS" << TC_WHITE << "" << std::endl;
    std::cout << TC_YELLOW << "         END" << TC_WHITE << "" << std::endl;
    std::cout << TC_YELLOW << "         HOME" << TC_WHITE << "" << std::endl;
    std::cout << TC_YELLOW << "         ENTER" << TC_WHITE << "" << std::endl;
    std::cout << TC_YELLOW << "         RETURN" << TC_WHITE << "" << std::endl;
    std::cout << "\n    Global functions " << std::endl;
    std::cout << TC_YELLOW << "         void Run()" << TC_WHITE << " Runs the loop() function if scrpts defines it." << std::endl;
    std::cout << TC_YELLOW << "         void Debug(1..3)" << TC_WHITE << " Sets debug level 0,1,2,3" << std::endl;
    std::cout << TC_YELLOW << "         $ System(cmd)" << TC_WHITE << " Runs a command shell, and returns i'ts output" << std::endl;
    std::cout << TC_YELLOW << "         SystemCb(cmd, (cb))" << TC_WHITE << " runs a commad in shell and for each output of the command line/chunk calls cb() function" << std::endl;
    std::cout << TC_YELLOW << "         Exit()" << TC_WHITE << " Exits when no activity on device. " << std::endl;
    std::cout << TC_YELLOW << "         Sleep(ms)" << TC_WHITE << " Sleeps ms milliseconds" << std::endl;
    std::cout << TC_YELLOW << "         int Str2num(string)" << TC_WHITE << " Converts \"0x12\" -> 18" << std::endl;
    std::cout << TC_YELLOW << "         $ Arr2str([])" << TC_WHITE << " Converts ['A','B'] - > \"AB\" assumed elements are bytes" << std::endl;
    std::cout << TC_YELLOW << "         [] Str2arr($)" << TC_WHITE << "  Converts\"0203\" -> [2,3]" << std::endl;
    std::cout << TC_YELLOW << "         $ Num2str(#,#)" << TC_WHITE << " Converts  (15,1) -> \"0x0F\"   15,2 -> \"0x000F\", Up to 8 bytes" << std::endl;
    std::cout << TC_YELLOW << "         [] Num2arr(#)" << TC_WHITE << " As the above function but returns an array of bytes. Up to 8 bytes" << std::endl;
    std::cout << TC_YELLOW << "         $ Arr2ascii([])" << TC_WHITE << "  Converts [] -> ASCII. Non printable are replaced with '.'" << std::endl;
    std::cout << TC_YELLOW << "         # Arr2rint([],1..8)"<< TC_WHITE << " Converts a uint8_t..uint64_t from bytes, Big Endian" << std::endl;
    std::cout << TC_YELLOW << "         # Arr2int([],1..8)"<< TC_WHITE << " Converts  builds a uint8_t up to uint64_t from bytes, Little Endian" << std::endl;
    std::cout << TC_YELLOW << "         $ SubArr2str([],start,count)" << TC_WHITE << " Converts  as Arr2Str form index from index an 'elements' bytes" << std::endl;
    std::cout << TC_YELLOW << "         # ArrFind([haystack],[needle])"<< TC_WHITE << " Search needle. Returns the index of match, or -1 if no match" << std::endl;
    std::cout << TC_YELLOW << "         # ArrEqual([],[])" << TC_WHITE << " memcmp() between contents, 0 if arrays are equal" << std::endl;

    std::cout << TC_YELLOW << "         Shell()" << TC_WHITE << " Opens a text raw shell to device. Messages are not routed" << std::endl;
    std::cout << TC_YELLOW << "         Device Dev($)" << TC_WHITE << " Returns device by name passed int he device constructor" << std::endl;
    std::cout << TC_YELLOW << "         Cin()" << TC_WHITE << "  Console in, The input is returned to script" << std::endl;
    std::cout << TC_YELLOW << "         Help()" << TC_WHITE << " Prints this help" << std::endl;
    std::cout << "\n    Device Object methods" << std::endl;
    std::cout << TC_YELLOW << "    Device($cs, $name, MODE)" << TC_WHITE << " Creates a device object, " << std::endl;
    std::cout << TC_WHITE  << "                  $cs - connection string is: 'protocol://specific_to_device' " << std::endl;
    std::cout << TC_WHITE  << "                     protcol:  tty, phydget, file  (To add: 'ssh,curl,sftp,qtui,https/http'')" << std::endl;
    std::cout << TC_WHITE  << "                       OK  tty         Serial device RS232"<< TC_YELLOW << " tty://COM1,115200,8N1" << std::endl;
    std::cout << TC_WHITE  << "                       OK  phy         phydge USB device"<< TC_YELLOW << " phydget://SERIAL,I/O" << std::endl;
    std::cout << TC_WHITE  << "                       OK  file        Regular disk file"<< TC_YELLOW << " file://C:\\myfile.txt" << std::endl;
    std::cout << TC_WHITE  << "                       OK  sftp/ssh    ssh and sftp"<< TC_YELLOW << " ssh://<user/<password>@>IP<:PORT>" << std::endl;
    std::cout << TC_WHITE  << "                       OK  http/https  http protocols"<< TC_YELLOW << " http<s>://address" << std::endl;
    std::cout << TC_WHITE  << "                       TBD qtui        user interface (TO SEE)"<< TC_YELLOW << " qtui://qmlfile.qml" << std::endl;
    std::cout << TC_WHITE  << "                  $name - a string id; used to retieve the device with Dev($) global function." << std::endl;
    std::cout << TC_WHITE  << "                          if string is null, loop() function's' first parameter is a device instance, otherwise is the device name" << std::endl;
    std::cout << TC_WHITE  << "                  MODE   = TEXT or BINARY" << std::endl;
    std::cout << TC_YELLOW << "All functions with return type '#' (number) return 0 for success. Otherwise might be the system error code." << std::endl;
    std::cout << TC_YELLOW << "All functions with return type '$' Can return null or an empty string" << std::endl;
    std::cout << TC_YELLOW << "All functions with return type '[]' Can return null or an empty array" << std::endl;
    std::cout << TC_YELLOW << "         seteol($)" << TC_WHITE << " Sets the end of line when putsnl() is called. Default eol is \"\\r\\n\" " << std::endl;
    std::cout << TC_YELLOW << "         putsnl($)" << TC_WHITE << " Sends text $ to device and appends EOL set up before.  " << std::endl;
    std::cout << TC_YELLOW << "         puts($)" << TC_WHITE << "  Sends test as is to device, except 'phidget device'" << std::endl;
    std::cout << TC_YELLOW << "         putsml($)" << TC_WHITE << " Sends a multiline text to device " << std::endl;
    std::cout << TC_YELLOW << "         write([])" << TC_WHITE << " Sends the array of bytes to the device" << std::endl;
    std::cout << TC_YELLOW << "         # reconnect(<to-ms>)" << TC_WHITE << "  Reconnects back to deivce. # is optional timeout. Returns 0 success, -1 no device available, 1- not connected/optional timeout" << std::endl;
    std::cout << TC_YELLOW << "         setbuff(#bytes)" << TC_WHITE << " Sets the maxim buffer to keep received data. Max 16000 bytes, Default 512" << std::endl;
    std::cout << TC_YELLOW << "         $ gets(<to-ms>)" << TC_WHITE << " With optional timeout, where you see <to-ms>, is optional timeout in mseconds " << std::endl;
    std::cout << TC_YELLOW << "         [] read(<to-ms>)" << TC_WHITE << "  " << std::endl;
    std::cout << TC_YELLOW << "         readmin(#bytes,<to-ms>)" << TC_WHITE << " Same as above but with optional timeout " << std::endl;
    std::cout << TC_YELLOW << "         issteady(#msecs)" << TC_WHITE << "  If the device does not produce any output for al least #for ms." << std::endl;
    std::cout << TC_YELLOW << "         true/false isopen()" << TC_WHITE << "  Returns true if device is open" << std::endl;
    std::cout << TC_YELLOW << "         open($constring)" << TC_WHITE << "  TO DO !!!" << std::endl;
    std::cout << TC_YELLOW << "         flushin()" << TC_WHITE << "  clears received data" << std::endl;
    std::cout << TC_YELLOW << "          # strexpect($exp, <to-ms>)" << TC_WHITE << " Expects in incoming data to see the 'exp', for optional timeout. Returns 0 if found." << std::endl;
    std::cout << TC_YELLOW << "           # binexpect([], <to-ms>)" << TC_WHITE << "  " << std::endl;
    std::cout << TC_YELLOW << "           # binexpect([])" << TC_WHITE << "  " << std::endl;
    std::cout << TC_YELLOW << "           # expectany(<to-ms>)" << TC_WHITE << "  Expects any data from device with custom optional timeout" << std::endl;
    std::cout << TC_YELLOW << "           [] writeread([],<to-ms>)" << TC_WHITE << " Write,Reads in one shot, with optional timeout" << std::endl;
    std::cout << TC_YELLOW << "           $ putsgets($, <to-ms>)" << TC_WHITE << "   Puts,gets in one shot with optional timeout" << std::endl;
    std::cout << TC_YELLOW << "           $ putsnlgets($, <to-ms>)" << TC_WHITE << "   Puts and appends sendcr<+LF> gets in one shot with optional timeout" << std::endl;
    std::cout << TC_YELLOW << "           # writeexpect([],[],<to-ms>)" << TC_WHITE << " Write and expect with optional timeouts. " << std::endl;
    std::cout << TC_YELLOW << "           # putsexpect($w,$e,<to-ms>)" << TC_WHITE << "  Same as above with strings" << std::endl;
    std::cout << TC_YELLOW << "           # putsnlexpect($w,$e,<to-ms>)" << TC_WHITE << "  " << std::endl;
    std::cout << TC_YELLOW << "           []readmin(bytes, <to-ms>ms>)"<< TC_WHITE << " Waits for minim bytes, returns all what was received" << std::endl;
    std::cout << TC_YELLOW << "           $ getsmin(chars, <to-ms>ms>)"<< TC_WHITE << " Waits for minim characters, returns all what was received" << std::endl;
    std::cout << TC_YELLOW << "         flush()" << TC_WHITE << "  Cleans slate, all i/o are cleared." << std::endl;
    std::cout << TC_YELLOW << "         setspeed(ms)" << TC_WHITE << " Bytes apart in milliscond, when sending them to device. Default is 0" << std::endl;
    std::cout << TC_YELLOW << "         setoptions($)" << TC_WHITE << " Custom to plugins if any specific  " << std::endl;
    std::cout << TC_YELLOW << "         settimeout(to-ms, selecttout)" << TC_WHITE << " Sets defaulttimeout for write/read" << std::endl;
    std::cout << TC_YELLOW << "         putctrl('$')" << TC_WHITE << " Likewise putctrl('C'), sends Control+C" << std::endl;
    std::cout << TC_YELLOW << "         close()" << TC_WHITE << " Closes the device. TODO" << std::endl;

    std::cout << TC_YELLOW << "         putscb($, cb)" << TC_WHITE << " Puts a string, then for any output calls the cb function" << std::endl;
    std::cout << TC_YELLOW << "         putsnlcb($, cb)" << TC_WHITE << " Puts a string+CRLF, then for any output calls the cb function" << std::endl;
    std::cout << TC_YELLOW << "         putsmlcb($), cb" << TC_WHITE << " Puts a multiline string, then for any output calls the cb function" << std::endl;
    std::cout << TC_YELLOW << "         writecb([], cb)" << TC_WHITE << " Writes the array then for any output calls the cb function" << std::endl;

    std::cout << TC_YELLOW << "         crexpect($, <to-ms>)" << TC_WHITE << " Sends Enter to device, then expects $ for # optional timeout" << std::endl;
    std::cout << TC_YELLOW << "         cr()" << TC_WHITE << " Sends Enter to device" << std::endl;

    std::cout << TC_YELLOW << " Procedures/Callbacks" << std::endl;
    std::cout << TC_YELLOW << "      int loop(dev, message, data)" << TC_WHITE << " Loop function would be called if is present, if main ends with Run()" << std::endl;
    std::cout << TC_WHITE << "              Called to indicate an event of a specific device." << std::endl;
    std::cout << TC_WHITE << "              return 1 to discard received data, 0 to hold it and be available  by a device read/gets, -1 to exit the script." << std::endl;
    std::cout << TC_WHITE << "              dev - would be a string, with device name if device constructor has a name != null" << std::endl;
    std::cout << TC_WHITE << "              message - CLOSED,IDLING,OPENED,BINARY,TEXT,ERCEIVING,SENDING" << std::endl;
    std::cout << TC_WHITE << "              data - data associated with the message" << std::endl;
    std::cout << TC_YELLOW << "      int cb(dev, message, data)" << TC_WHITE << " callback function signature in all cb calls" << std::endl;
    std::cout << TC_WHITE << "              Called to indicate all events on the device the cb call was done." << std::endl;
    std::cout << TC_WHITE << "              Applies to putscb, putsnlcb putsmlcb writecb." << std::endl;















    return 0;
}
