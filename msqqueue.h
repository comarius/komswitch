#ifndef MSGQUEUE_H_
#define MSGQUEUE_H_

#include <stdio.h>
#include <ctype.h>
#include <deque>
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <sqrat.h>
#include "icombase.h"
#include "osthread.h"
#include "sqwrap.h"
#include "plugins.h"

typedef std::basic_string<uint8_t>  bstring;

//.Const("DATA", 1)
//.Const("EVENT", 2)
//.Const("CLOSED", -1)
//.Const("EXIT", -1)
//.Const("IDLE", 0)

class  IComm;
class  Device;
struct  DevMsg
{
    size_t           _id;
    Device*          _pc;
    int              _msg;
    bstring          _data;
    Sqrat::Function  _func;

    DevMsg(Device* pc, int m,
           cbyte* d=0,
           size_t l=0):_id(++_Id),_pc(pc),_msg(m)
    {
        if(l)_data.append(d, l);
    }
    void setFunc(const Sqrat::Function& pf){_func = pf;}
    void setData(const uint8_t* pdata, size_t len){_data.clear(); _data.append(pdata,len);};
    void append(const DevMsg* pdata){_data.append(pdata->_data.data(),pdata->_data.length());};
    static size_t   _Id;
};

#define MAX_QSZ 1024
class MsgQueue
{
public:
    MsgQueue(){}
    ~MsgQueue()
    {
        _c.lock();
        while(_q.size()) {
            _q.pop_front();
        }
        _c.unlock();
    }
    int push(CntPtr<DevMsg>& c)
    {
        _c.lock();
        if(_q.size()<MAX_QSZ)
        {
            _q.push_back(c);
            _c.signal();
        }
        else{
            LOGERR("In Queue overflow");
        }
        _c.unlock();
         return 1;
    }

    int rpush(CntPtr<DevMsg>& c)
    {
        _c.lock();
        if(_q.size()<MAX_QSZ)
        {
            _q.push_front(c);
        }
        else{
            LOGERR("In Queue overflow");
        }
        _c.unlock();
         return 1;
    }

    CntPtr<DevMsg> pop(bool& ok)
    {
        CntPtr<DevMsg> r;
        ok = false;
        _c.lock();
        if(_q.size()) {
            r = _q.front();
            ok = true;
            _q.pop_front();
        }
        _c.unlock();
        if(_q.size())
            _c.signal();
        return r;
    }

    void remove(Device* pm)
    {
        _c.lock();
AGAIN:
        std::deque<CntPtr<DevMsg> >::iterator it = _q.begin();
        for(; it != _q.end(); it++)
        {
            if(it->ptr()->_pc == pm)
            {
                _q.erase(it);
                goto AGAIN;
            }
        }
        _c.unlock();
    }

    void signal() {
        _c.signal();
    }
    void broadcast() {
        _c.broadcast();
    }
    size_t size()const {
        return _q.size();
    }
private:
    std::deque<CntPtr<DevMsg> > _q;
    conditio _c;
};


#endif //MSGQUEUE_H_

