

#include <unistd.h>
#include <string>
#include <deque>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "sqwrap.h"
#include "sqratConst.h"
#include "icombase.h"
#include "divais.h"
#include "serialcom.h"
#include "plugins.h"
#include "main.h"
#include <libusb.h>


libusb_context          *_ctx;

#ifdef __linux
#include <set>
#include <sys/ioctl.h>
#include <termios.h>
#include <libgen.h>
#else
#include <stdlib.h>
#include <stdio.h>

#endif


RunCtx*             PCTX = 0;
int                 _Debug = 0;
std::set<Device*>   RunCtx::_Devices;
bool                RunCtx::_Alive = false;
SqEnv*              RunCtx::_Penv = 0;
MsgQueue            RunCtx::_MsgQue;
bool                RunCtx::_ShellOn = false;
bool                RunCtx::_looPing = false;
bool                RunCtx::_InScript = false;
int                 RunCtx::_LoopSpin = 128;
size_t              DevMsg::_Id=0;
Sqrat::Function     RunCtx::_funcloop;
CntPtr<DevMsg>*     RunCtx::_curMsg = 0;
std::map<std::string,std::string> RunCtx::_threads;
std::string         RunCtx::_LogFile;
long                RunCtx::_LogFileSz=4000000; // 4M
int                 RunCtx::_LogFiles=10;       // days behind
long                RunCtx::_LogFileLastSz=0;
SqEnv*              PSq = 0;
SoEntry*            RunCtx::_plugin;

class TheClass
{
public:
    TheClass(int dummy){

    }

    ~TheClass(){

    }


    static void StaticFunction(Sqrat::Function f)
    {
        std::cout <<__FUNCTION__ << std::endl;
        f.Evaluate<int>(" ---calling from static\n");
    }

    void Function(Sqrat::Function f)
    {
        std::cout <<__FUNCTION__ << std::endl;
        f.Evaluate<int>(" ---calling from method\n");
    }


};



/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
    SqEnv               sq;
    std::string         script;
/**
    HSQUIRRELVM         VM = sq.theVM();
    char                di[128]={0};

    sq.acquire();
    RunCtx::_Penv = &sq;

    Sqrat::RootTable(VM).Func("StaticFunction", &TheClass::StaticFunction);

    Sqrat::Class<TheClass> cls(sq.theVM(), _SC("TheClass"));

    cls.Func(_SC("Function"), &TheClass::Function);
    cls.Ctor<int>();
    Sqrat::RootTable().Bind(_SC("TheClass"), cls);

    try{
        MyScript scr = sq.compile_script("./test.sss");
        scr.run_script();
        Sqrat::Function f = Sqrat::RootTable().GetFunction(_SC("main"));
        if(!f.IsNull())
        {
            f.Evaluate<int>();
        }
    }
    catch(Sqrat::Exception ex)
    {
        LOGERR("Main: " << ex.Message());
        ::msleep(50000);
    }

    return 0;
*/

    PSq =&sq;
    std::cout << "Autcom Ver: 2.0.1-"<<__DATE__<<"\r\n";

    if(argc != 2)
    {
        std::cout << "enter a script name >";
        std::cin >> script;
    }
    else
    {
        script = argv[1];
    }

    RunCtx::_Alive  =   true;
    PCTX = new RunCtx();
    sq.acquire();
    RunCtx::_Penv = &sq;

    Sqrat::ConstTable(sq.theVM())
            .Const("CLOSED", (int)IDevice::eCLOSED)
            .Const("IDLING", (int)IDevice::eIDLE)
            .Const("IDLE", (int)IDevice::eIDLE)
            .Const("OPENED", (int)IDevice::eOPENDED)
            .Const("BINARY", (int)IDevice::eBINARY)
            .Const("TEXT", (int)IDevice::eTEXT)
            .Const("NUMBER", (int)IDevice::eNUMBER)
            .Const("OK", (int)0)
            .Const("DISCARD", (int)IDevice::eDISCARD)
            .Const("WAITING", (int)IDevice::eWAITING)
            .Const("RECEIVING", (int)IDevice::eRECEIVING)
            .Const("ERROR", (int)IDevice::eERROR)
            .Const("SENDING", (int)IDevice::eSENDING)
            .Const("HANG", (int)IDevice::eHANG)
            .Const("INFINITE",31536000)
            .Const("PROCESED", 1)
            .Const("CONTINUE", 0)
            .Const("EXIT", -1)
            .Const("ADAY", 86400000)
            .Const("AYEAR",31536000)
            .Const("F1", "1B4F50")
            .Const("UP", "1B5B41")
            .Const("DOWN", "1B5B42")
            .Const("LEFT", "1B5B44")
            .Const("RIGHT", "1B5B43")
            .Const("DEL", "1B5B33")
            .Const("INS", "1B5B32")
            .Const("END", "1B4F46")
            .Const("HOME", "1B4F48")
            .Const("ENTER", "0D")
            .Const("RETURN", "0A")
            .Const("EXIT",    -1)
            .Const("PROCESSED", 1)
            .Const("SYNC", 1)
            .Const("ASYNC", 2);

    Sqrat::RootTable(sq.theVM()).Func("Run", &RunCtx::Run);
    Sqrat::RootTable(sq.theVM()).Func("Debug", &RunCtx::setdebug);
    Sqrat::RootTable(sq.theVM()).Func("Exit", &RunCtx::exitonidle);
    Sqrat::RootTable(sq.theVM()).Func("Error", &RunCtx::Error);
    Sqrat::RootTable(sq.theVM()).Func("Delete", &RunCtx::Delete);
    Sqrat::RootTable(sq.theVM()).Func("System", &RunCtx::System);
    Sqrat::RootTable(sq.theVM()).Func("Notify", &RunCtx::Notify);
    Sqrat::RootTable(sq.theVM()).Func("SystemCb", &RunCtx::SystemCb);
    Sqrat::RootTable(sq.theVM()).Func("Sleep", &RunCtx::XSleep);
    Sqrat::RootTable(sq.theVM()).Func("Tick", &RunCtx::Tick);
    Sqrat::RootTable(sq.theVM()).Func("Str2num", &RunCtx::str2num);   // "0x12" - > 18
    Sqrat::RootTable(sq.theVM()).Func("Arr2str", &RunCtx::arr2str);   // [1,3,4] - > 010204

    Sqrat::RootTable(sq.theVM()).Func("DbgArr", &RunCtx::DbgArr);   // [1,3,4] - > 010204

    Sqrat::RootTable(sq.theVM()).Func("Str2arr", &RunCtx::str2arr);   // "01020304" - > [1,2,4,4]
    Sqrat::RootTable(sq.theVM()).Func("Num2str", &RunCtx::num2str);   // 5,2 - > 05,  5,4 - > 0005
    Sqrat::RootTable(sq.theVM()).Func("Num2arr", &RunCtx::num2arr);     // 512,4 - > [0,0,1,ff]
    Sqrat::RootTable(sq.theVM()).Func("Arr2ascii", &RunCtx::arr2ascii); // 512,4 - > [0,0,1,ff]

    Sqrat::RootTable(sq.theVM()).Func("Shell", &RunCtx::shellio);
    Sqrat::RootTable(sq.theVM()).Func("Dev", &RunCtx::dev);
    Sqrat::RootTable(sq.theVM()).Func("WaitDevices", &RunCtx::WaitDevices);
    Sqrat::RootTable(sq.theVM()).Func("Cin", &RunCtx::coin);
    Sqrat::RootTable(sq.theVM()).Func("Idle", &RunCtx::idle);
    Sqrat::RootTable(sq.theVM()).Func("Help", &RunCtx::help);

    Sqrat::RootTable(sq.theVM()).Func("Arr2rint", &RunCtx::arr2rint);  //
    Sqrat::RootTable(sq.theVM()).Func("Arr2int", &RunCtx::arr2int);    //
    Sqrat::RootTable(sq.theVM()).Func("SubArr2str", &RunCtx::parr2str);  //
    Sqrat::RootTable(sq.theVM()).Func("ArrFind", &RunCtx::ArrFind);
    Sqrat::RootTable(sq.theVM()).Func("ArrEqual", &RunCtx::ArrEqual);
    Sqrat::RootTable(sq.theVM()).Func("Looping", &RunCtx::Looping);
    Sqrat::RootTable(sq.theVM()).Func("EnableLog", &RunCtx::EnableLog);
    Sqrat::RootTable(sq.theVM()).Func("Log", &RunCtx::Log);

    //Sqrat::RootTable(sq.theVM()).Func("Plug", &RunCtx::Plug);


    Sqrat::Class<Device> cls(sq.theVM(), _SC("Device"));
    cls.Ctor<const char*, const char*, int>();
    cls.Ctor();
    cls.Func(_SC("testcb"), &Device::testcb);
    cls.Func(_SC("publish"), &Device::publish);
    cls.Func(_SC("seteol"), &Device::seteol);
    cls.Func(_SC("write"), &Device::write);
    cls.Func(_SC("reconnect"), &Device::reconnect);
    cls.Func(_SC("waitonline"), &Device::waitOnline);
    cls.Func(_SC("setbuff"), &Device::setbuff);
    cls.Func(_SC("flushin"), &Device::flushin);

    cls.Func(_SC("putsnl"), &Device::putsnl);
    cls.Func(_SC("puts"), &Device::puts);
    cls.Func(_SC("putsml"), &Device::putsml);

    cls.Overload<const char* (Device::*)()>(_SC("gets"), &Device::gets);
    cls.Overload<const char* (Device::*)(int)>(_SC("gets"), &Device::gets);

    cls.Overload<const char* (Device::*)(int)>(_SC("getsmin"), &Device::getsmin);
    cls.Overload<const char* (Device::*)(int)>(_SC("getsmin"), &Device::getsmin);

    cls.Overload<Sqrat::Array  (Device::*)()>(_SC("read"), &Device::read);
    cls.Overload<Sqrat::Array  (Device::*)(int)>(_SC("read"), &Device::read);

    cls.Overload<Sqrat::Array   (Device::*)(int)>(_SC("readmin"), &Device::readmin);
    cls.Overload<Sqrat::Array   (Device::*)(int,int)>(_SC("readmin"), &Device::readmin);

    cls.Func(_SC("issteady"), &Device::issteady);
    cls.Func(_SC("isopen"), &Device::isopen);
    cls.Func(_SC("printout"), &Device::printout);
    cls.Func(_SC("open"), &Device::open);
    cls.Func(_SC("sent"), &Device::sent);
    cls.Func(_SC("escape"), &Device::escape);
    cls.Func(_SC("autoflush"), &Device::autoflush);

    cls.Overload<int (Device::*)(const char*,int)>(_SC("strexpect"), &Device::strexpect);
    cls.Overload<int (Device::*)(const char*)>(_SC("strexpect"), &Device::strexpect);

    cls.Overload<int (Device::*)(Sqrat::Array,int)>(_SC("binexpect"), &Device::binexpect);
    cls.Overload<int (Device::*)(Sqrat::Array)>(_SC("binexpect"), &Device::binexpect);

    cls.Overload<Sqrat::Array (Device::*)(Sqrat::Array)>(_SC("writeread"), &Device::writeread);
    cls.Overload<Sqrat::Array (Device::*)(Sqrat::Array,int)>(_SC("writeread"), &Device::writeread);

    cls.Overload<const char* (Device::*)(const char*)>(_SC("putsgets"), &Device::putsgets);
    cls.Overload<const char* (Device::*)(const char*,int)>(_SC("putsgets"), &Device::putsgets);

    cls.Overload<const char* (Device::*)(const char*)>(_SC("putsnlgets"), &Device::putsnlgets);
    cls.Overload<const char* (Device::*)(const char*,int)>(_SC("putsnlgets"), &Device::putsnlgets);

    cls.Overload<int (Device::*)(Sqrat::Array,Sqrat::Array)>(_SC("writeexpect"), &Device::writeexpect);
    cls.Overload<int (Device::*)(Sqrat::Array,Sqrat::Array,int)>(_SC("writeexpect"), &Device::writeexpect);

    cls.Overload<int (Device::*)(const char*,const char*)>(_SC("putsexpect"), &Device::putsexpect);
    cls.Overload<int (Device::*)(const char*,const char*,int)>(_SC("putsexpect"), &Device::putsexpect);

    cls.Overload<int (Device::*)(const char*,const char*)>(_SC("putsnlexpect"), &Device::putsnlexpect);
    cls.Overload<int (Device::*)(const char*,const char*,int)>(_SC("putsnlexpect"), &Device::putsnlexpect);


    cls.Overload<int (Device::*)(const char*)>(_SC("sendkey"), &Device::sendkey);
    cls.Overload<int (Device::*)(const char*, Sqrat::Function)>(_SC("sendkeycb"), &Device::sendkeycb);
    cls.Overload<int (Device::*)(const char*,const char*)>(_SC("sendkeyexpect"), &Device::sendkeyexpect);
    cls.Overload<int (Device::*)(const char*,const char*,int)>(_SC("sendkeyexpect"), &Device::sendkeyexpect);

    cls.Func(_SC("putscb"), &Device::putscb);
    cls.Func(_SC("putsnlcb"), &Device::putsnlcb);
    cls.Func(_SC("putsmlcb"), &Device::putsmlcb);
    cls.Func(_SC("writecb"), &Device::writecb);

    cls.Func(_SC("name"), &Device::name);


    cls.Overload<int (Device::*)(int)>(_SC("expectany"), &Device::expectany);
    cls.Overload<int (Device::*)()>(_SC("expectany"), &Device::expectany);
    cls.Func(_SC("flush"), &Device::flush);
    cls.Func(_SC("setspeed"), &Device::setspeed);
    cls.Func(_SC("setoptions"), &Device::setoptions);
    cls.Func(_SC("settimeout"), &Device::settimeout);
    cls.Func(_SC("setbinary"), &Device::setbinary);

    cls.Func(_SC("putctrl"), &Device::putctrl);
    cls.Func(_SC("close"), &Device::close);

    cls.Func(_SC("remove"), &Device::removeFile);


    Sqrat::RootTable().Bind(_SC("Device"), cls);

#ifdef __linux

    RunCtx::_LogFile = "/var/log/autocom_log";

    if(::access(script.c_str(),0) != 0)
#else

    if(0)

#endif
    {
        char cwd[256];
        ::getcwd(cwd,255);
        std::cout << "Cannot open:" << script << " from current wd: " << cwd << std::endl;
        return 0;
    }

    try{

        int r = libusb_init(&_ctx);
        if(r < 0)
        {
            std::cout<<"Init Error "<<r<<std::endl;
            return 0;
        }

        LOGDETAIL2("Compiling " << script);
        MyScript scr = sq.compile_script(script.c_str());
        LOGDETAIL2("Running " << script);
        scr.run_script();
        RunCtx::_funcloop = Sqrat::RootTable().GetFunction(_SC("loop"));
        if(RunCtx::_funcloop.IsNull())
        {
            LOGWARN("function 'var loop(dev, msg, data)' not found");
        }
        Sqrat::Function f = Sqrat::RootTable().GetFunction(_SC("main"));
        if(!f.IsNull())
        {
            RunCtx::_looPing = false;
            LOGDETAIL2("Calling main())");
            f.Evaluate<int>(script.c_str());
            LOGDETAIL2("\nMain Ended");
        }
        else
        {
            LOGWARN("function 'var main(app)' not found ");
        }

        if(_ctx)
           libusb_exit(_ctx);
    }
    catch(Sqrat::Exception ex)
    {
        LOGERR("Main: " << ex.Message());
        ::msleep(50000);
    }
    RunCtx::_looPing = false;
    if(!RunCtx::_funcloop.IsNull())
    {
        RunCtx::_funcloop.Release();
    }
    RunCtx::_Alive = false;
    LOGDETAIL2("\nRunning context ended");


    //
    // script should have destroy all instances already...
    //
    for(auto& a : (RunCtx::_Devices))
    {
        delete a;
    }
    delete PCTX;
    std::cout << "\r\nBYE\r\n";
#ifdef DEBUG
    std::cout << "Press a key then enter to exit \n";
    getchar();
#endif

    return 0;
}

/**
 * @brief RunCtx::sexecute, not used
 * @param aprog
 * @return
 */
bool RunCtx::sexecute(std::vector<std::string> & aprog)
{
    std::string prog;
    std::vector<std::string>::iterator b = aprog.begin();
    for(;b!=aprog.end();b++)
        prog.append(*b);

    sq_compilebuffer(_Penv->theVM(), prog.c_str(), SQInteger(prog.length()), "", SQTrue);

    sq_pushroottable(_Penv->theVM());
    if (SQ_FAILED(sq_call(_Penv->theVM(), 1, SQFalse, SQFalse)))
    {
        const SQChar* errMsg = "Unknown error.";
        sq_getlasterror(_Penv->theVM());
        if (sq_gettype(_Penv->theVM(), -1) == OT_STRING)
        if (sq_gettype(_Penv->theVM(), -1) == OT_STRING)
            sq_getstring(_Penv->theVM(), -1, &errMsg);
        LOGERR (errMsg);
        return false;
    }
    else
        return true;
}

/**
 * @brief RunCtx::Exit
 * @param slp
 * @return
 */
int RunCtx::Exit(int slp)
{
    std::set<Device*>::iterator it;

    it =_Devices.begin();
    for(; it != _Devices.end(); ++it)
    {
        Device* pc = *it;
        if(pc->isopen()){
            pc->close();
            pc->signal_to_stop();
        }
    }
    _Alive=false;
    return slp;
}


/**
 * @brief RunCtx::Tick
 * @param t
 */
int RunCtx::Tick()
{
    return ::tick_count();
}

/**
 * @brief RunCtx::XSleep
 * @param t
 */
void RunCtx::XSleep(int t)
{
    size_t fut = ::tick_count() + t, ct=::tick_count();

    while(ct < fut && RunCtx::_Alive)
    {
        ct=::tick_count();
        ::msleep(4);
    }
}

/**
 * @brief RunCtx::num2str
 * @param hex
 * @param bytes
 * @return
 */
const char* RunCtx::num2str(int hex, int bytes)
{
    static char out[8];
    switch(bytes)
    {
        case 1:
            ::sprintf(out,"%02X", hex);
            break;
        case 2:
            ::sprintf(out,"%04X", hex);
            break;
        case 4:
            ::sprintf(out,"%08X", hex);
            break;
        case 8:
            ::sprintf(out,"%016X", hex);
            break;
    }
    LOGDETAIL2 (__FUNCTION__ << ":  "  << out);
    return out;
}

/**
 * @brief RunCtx::ArrFind
 * @param a
 * @param b
 * @return
 */
int RunCtx::ArrFind(Sqrat::Array a, Sqrat::Array b)
{
    int     sza = a.GetSize();
    uint8_t ptra[1024];
    int     szb = b.GetSize();
    uint8_t ptrb[1024];

    a.GetArray(ptra, sza);
    b.GetArray(ptrb, szb);
    if(sza>=szb)
    for(int k=0;k<=(sza-szb);k++)
    {
        if(::memcmp(ptra+k,ptrb,szb)==0)
        {
            LOGDETAIL2 (__FUNCTION__ << " subarray found at index: " << k);
            return k;
        }
    }
    LOGDETAIL2 (__FUNCTION__ << " subarray not found");
    return -1;
}

/**
 * @brief RunCtx::ArrEqual
 * @param a
 * @param b
 * @return
 */
int RunCtx::ArrEqual(Sqrat::Array a, Sqrat::Array b)
{
    int     sza = a.GetSize();
    int     szb = b.GetSize();

    uint8_t ptra[8912];
    uint8_t ptrb[8912];

    a.GetArray(ptra, sza);
    b.GetArray(ptrb, szb);
    if(sza==szb)
    {
        if(0==::memcmp(ptra,ptrb,szb))
        {
            LOGDETAIL2 (__FUNCTION__ << " arrays are equal ");
            return 0;
        }
        return 1;
    }
    LOGDETAIL2 (__FUNCTION__ << " arrays are NOT equal ");
    return sza-szb;
}

/**
 * @brief RunCtx::arr2ascii
 * @param a
 * @return
 */
const char* RunCtx::arr2ascii(Sqrat::Array a)
{
    static std::string out;

    int     sz = a.GetSize();
    uint8_t ptr[1024];
    char    duplet[4];

    out.clear();
    a.GetArray(ptr, sz);
    for(int i=0;i<sz;i++)
    {
        if(isprint(int(ptr[i])) ||
           ptr[i]==10|| ptr[i]==13 )
            ::sprintf(duplet,"%c",int(ptr[i]));
        else
            duplet[0]='.';
        out.append(duplet);
    }
    LOGDETAIL2 (__FUNCTION__ << ":  " << out);
    return out.c_str();
}

/**
 * @brief RunCtx::DbgArr
 * @param a
 * @param how
 * @return
 */
const char* RunCtx::DbgArr(Sqrat::Array a, int how)
{
    static std::string out;
    static std::string chout;

    int     sz = a.GetSize();
    uint8_t ptr[1024];
    char    duplet[4];
    char    charr[4];

    out.clear();
    chout.clear();
    a.GetArray(ptr, sz);
    for(int i=0;i<sz;i++)
    {
        ::sprintf(duplet,"%02X ",int(ptr[i]));
        if(::isprint(int(ptr[i])))
            ::sprintf(charr,"%c  ",int(ptr[i]));
        else
            ::sprintf(charr,"%c  ",'.');
        out.append(duplet);
        chout.append(charr);
    }
    std::cout << TC_YELLOW  << std::endl;
    std::cout << out <<  "   " << sz <<" bytes" << std::endl;
    if((how & 0x1) == 0x1)
        std::cout << chout << std::endl;
    std::cout << TC_WHITE<< std::flush;
    return out.c_str();
}

/**
 * @brief RunCtx::arr2str
 * @param a
 * @return
 */
const char* RunCtx::arr2str(Sqrat::Array a, char sep)
{
    static std::string out;

    int     sz = a.GetSize();
    uint8_t ptr[1024];
    char    duplet[4];

    out.clear();
    a.GetArray(ptr, sz);
    for(int i=0;i<sz;i++)
    {
        ::sprintf(duplet,"%02X%c",int(ptr[i]),sep);
        out.append(duplet);
    }
    LOGDETAIL2 (__FUNCTION__ << ":  " <<  out);
    return out.c_str();
}

/**
 * @brief RunCtx::arr2int
 * @param a
 * @param from
 * @param bytes
 * @return
 */
int RunCtx::arr2int(Sqrat::Array a, int from, int bytes)
{
    int     ret = 0;
    int     sz = a.GetSize();
    uint8_t ptr[1024];
    int shift = 0;
    a.GetArray(ptr, sz);
    for(int i=from;i < (from+bytes);i++)
    {
        int val = ptr[i];
        val <<= shift;
        ret |= val;
        shift+=8;
    }
    LOGDETAIL2 (__FUNCTION__ << ":  " <<  ret);
    return ret;
}

/**
 * @brief RunCtx::arr2rint
 * @param a
 * @param from
 * @param bytes
 * @return
 */
int RunCtx::arr2rint(Sqrat::Array a, int from, int bytes)
{
    int     ret = 0;
    int     sz = a.GetSize();
    uint8_t ptr[1024];
    int shift = 8*(bytes-1);
    a.GetArray(ptr, sz);
    for(int i=from;i < (from+bytes);i++)
    {
        int val = ptr[i];
        val <<= shift;
        ret |= val;
        shift -=8;
    }
    LOGDETAIL2 (__FUNCTION__ << ":  " <<  ret);
    return ret;
}

/**
 * @brief RunCtx::parr2str
 * @param a
 * @param from
 * @param len
 * @return
 */
const char* RunCtx::parr2str(Sqrat::Array a, int from, int len, char sep)
{
    static std::string out;

    int     sz = a.GetSize();
    uint8_t ptr[1024];
    char    duplet[16];

    out.clear();
    a.GetArray(ptr, sz);
    for(int i=from;i<(from+len);i++)
    {
        ::sprintf(duplet,"%02X%c",int(ptr[i]),sep);
        out.append(duplet);
    }
    LOGDETAIL2 (__FUNCTION__ << ":  " <<  out);
    return out.c_str();
}

/**
 * @brief RunCtx::str2num
 * @param str
 * @return
 */
int RunCtx::str2num(const char* str)
{
    int val = (int)strtol(str, NULL, 0);
    LOGDETAIL2 (__FUNCTION__ << ":  " <<  std::hex <<", "<< val <<std::dec);
    return val;
}

/**
 * @brief RunCtx::str2arr
 * @param str
 * @return
 */
Sqrat::Array RunCtx::str2arr(const char* str)
{
    char         tmp[32]={0};
    int          val;
    size_t       sl = ::strlen(str);
    Sqrat::Array ra(_Penv->theVM(),sl/2);
    int          advance = 0;

    LOGDETAIL2 (__FUNCTION__ << ":  " <<  str);
    for(size_t idx = 0 ; idx < sl; idx+=2)
    {
        ::strncpy(tmp, &str[idx],2);
        ::sscanf(tmp,"%02X",&val);
        ra.SetValue(advance, uint8_t(val));
        ++advance;
    }
    return ra;
}

/**
 * @brief RunCtx::num2arr
 * @param hex
 * @param bytes
 * @return
 */
Sqrat::Array RunCtx::num2arr(int hex, int bytes)
{
    char out[32];

    switch(bytes)
    {
        case 1:
            ::sprintf(out,"%02X", hex);
            break;
        case 2:
            ::sprintf(out,"%04X", hex);
            break;
        case 4:
            ::sprintf(out,"%08X", hex);
            break;
        case 8:
            ::sprintf(out,"%016X", hex);
            break;
    }
    LOGDETAIL2 (__FUNCTION__ << ":  " <<  out);
    char         tmp[16]={0};
    int          val;
    size_t       sl = ::strlen(out);
    Sqrat::Array ra(_Penv->theVM(),sl/2);
    int          advance = 0;

    for(size_t idx = 0 ; idx < sl; idx+=2)
    {
        ::strncpy(tmp, &out[idx],2);
        ::sscanf(tmp,"%02X",&val);
        ra.SetValue(advance, uint8_t(val));
        ++advance;
    }
    return ra;
}

/**
 * @brief RunCtx::toString
 * @param binary
 * @param len
 * @return
 */
std::string  RunCtx::toString(cbyte* binary, int len)
{
    static std::string chunk;
    char        duplet[4];

    chunk.clear();
    for(int i=0;i<len;i++)
    {
        ::sprintf(duplet,"%02X",int(binary[i]));
        chunk.append(duplet);
    }
    return chunk;
}

/**
 * @brief RunCtx::toBinary
 * @param string
 * @return
 */
bstring  RunCtx::toBinary(const char* string)
{
    static bstring  binstr;
    int      val;
    char     tmp[4]={0};

    binstr.clear();
    size_t l = ::strlen(string);
    for(size_t c=0;c<l;c+=2)
    {
        ::strncpy(tmp,&string[c],2);
        ::sscanf(tmp,"%02X",&val);
        binstr.append(1, char(val));
    }
    return binstr;
}

/**
 * @brief RunCtx::toBinary
 * @param string
 * @return
 */
bstring  RunCtx::toBinary2(const char* string)
{
    static bstring  binstr;
    int      val;
    char     tmp[4]={0};

    binstr.clear();
    size_t l = ::strlen(string);
    for(size_t c=0;c<l;c+=2)
    {
        ::strncpy(tmp,&string[c],2);
        ::sscanf(tmp,"%02X",&val);
        binstr.append(1, char(val));
    }
    return binstr;
}

/**
 * @brief RunCtx::setdebug
 * @param deb
 */
void RunCtx::setdebug(int deb)
{
    _Debug = deb;
    std::set<Device*>::iterator it =_Devices.begin();
    for(; it != _Devices.end(); ++it)
    {
        (*it)->setdebug(deb);
    }
}

/**
 * @brief RunCtx::Delete
 * @param p
 */
void RunCtx::Delete(Device* p)
{
    delete p;
}

/**
 * @brief RunCtx::Run
 * @return
 */
int RunCtx::Run()
{
    int rv = 0;
    _looPing = true;
    try{
        rv = RunCtx::_run(1);
    }
    catch(Sqrat::Exception ex)
    {
        LOGERR( ex.Message());
        RunCtx::_Alive = false;
    }
    _looPing = false;
    return rv;
}

/**
 * @brief RunCtx::dev
 * @param name
 * @return
 */
Device* RunCtx::dev(const char* name)
{
    std::set<Device*>::iterator it =_Devices.begin();
    for(; it != _Devices.end(); ++it){
        IDevice* pc = *it;
        if(!::strcmp(pc->name(),name))
            return (Device*)pc;
    }
    LOGERR("Dev (\""<<name<<"\"')" <<  " Not found. Returning null instance");
    return 0;
}

/**
 * @brief RunCtx::coin
 * @return
 */
const char* RunCtx::coin()
{
    Autoreset a(&_ShellOn);
    std::cout << std::endl <<"cin>> " << std::flush;
    static std::string out;
    out.clear();
    std::cin >> out;
    return out.c_str();
}

/**
 * @brief RunCtx::shellio
 */
void RunCtx::shellio()
{
    bool    binary = false;
    uint8_t outbuff[256];
    Device* pcd;
    std::set<Device*>::iterator it =_Devices.begin();
    std::cout << "- connected devices: " << std::endl;
    int ndevice = 0;
    std::vector<Device*> pdd;
    for(; it != _Devices.end(); ++it){
        Device* pc = *it;
        std::cout << ndevice <<": " << std::hex << pc << std::dec <<": "<< pc->name()<< std::endl;
        pdd.push_back(pc);
        ++ndevice;
    }
    std::string input;
    std::cout << "select a device by index.\n";
    std::cin >> input;
    int nindex = std::stoi(input);
    if(nindex <= pdd.size()-1)
    {
        pcd = pdd[nindex];
    }
    else
    {
        std::cout << "out of range \n";
        return ;
    }
    binary=pcd->mode() & IDevice::eBINARY;
    _ShellOn = true;
    while(true)
    {
        std::string input;
        if(binary)
            std::cout << "bin> " << std::flush;
        else
            std::cout << "txt> " << std::flush;

        std::cin >> input;
        if(input=="~q")
            break;
        if(input=="~b")
        {
            binary=true;
            continue;
        }
        if(input=="~t")
        {
            binary=false;

            continue;
        }
        if(input.length())
        {
            if(pcd)
            {
                if(binary)
                {
                    bstring bin = toBinary(input.c_str());
                    pcd->binwrite(bin.data(), bin.length());
                }
                else
                {
                    pcd->binwrite((cbyte*)input.c_str(), input.length());
                }
                ::usleep(128000);

            }
        }
    }
    std::cout << "shell ended"<< std::endl;
    _ShellOn = false;
}

/**
 * @brief RunCtx::loopit
 * @return
 */
int RunCtx::loopit()
{
//    if(!RunCtx::_looPing)
//        return RunCtx::_Alive;
    if(!RunCtx::_InScript)
        return _run(0, true);
    return RunCtx::_Alive;
}

/**
 * @brief RunCtx::idle
 * @return
 */
bool RunCtx::idle()
{
    if(!RunCtx::_looPing)
        return RunCtx::_Alive;
    if(!RunCtx::_InScript)
        return _run(8, false)==-1 ? -1 : 1;
    return RunCtx::_Alive;
}

/**
 * @brief RunCtx::exitonidle
 * @return
 */
int RunCtx::exitonidle()
{
    while(RunCtx::_MsgQue.size())
    {
        idle();
    }
    RunCtx::_Alive = false;
    return 0;
}

/**
 * @brief RunCtx::_run
 * @param interval
 * @param loop
 * @return
 */
int RunCtx::_run(int interval, bool loop)
{
    static  int     counter = 1;
    int             rv=0;
    bool            idling=false;
    char            round[]="|/-\\|/-\\*0";
    bool            sent = false;
    bool            provided;
    std::string     thismsg;
    size_t          idldetick = ::tick_count();
    size_t          startt = ::tick_count();
    CntPtr<DevMsg>  lastMsg;
    size_t          lastMsgPoke=0;
    std::vector<size_t>  ids;

    do{
        idling = true;
        sent = false;
        thismsg = "spin ";
        while(rv!=-1) // drain it
        {
            ids.clear();
            CntPtr<DevMsg>  msg = _MsgQue.pop(provided);
            if(!provided)
                break;
            ids.push_back(msg->_id);
/*
            do{
                CntPtr<DevMsg>  msgck = _MsgQue.pop(provided);
                if(provided){
                    msg->append(msgck.ptr());
                    ids.push_back(msgck->_id);
                }
            }while(provided);
*/

            RunCtx::_curMsg = &msg;
            rv = 0;
            if(!msg->_func.IsNull())
            {
                LOGDETAIL2("calling callback()");
                thismsg = "cust ";
                rv = RunCtx::callSqFoo(msg->_pc , msg->_func, msg);
                lastMsg = msg;
                lastMsgPoke = 0;
            }
            else
            {
                if(!_funcloop.IsNull())
                {
                    LOGDETAIL2("calling loop()");
                    thismsg = "loop ";
                    rv = RunCtx::callSqFoo(msg->_pc,  _funcloop, msg);
                }
            }
            /*
            if(rv == 0) //put it back
            {
                _MsgQue.rpush(msg);
            }
            */
            ((Device*)msg->_pc)->discardDevData(ids, rv);
            idling = false;
            sent   = true;
            startt = ::tick_count();
            RunCtx::_curMsg = 0;
        }
        if(sent==false)
        {
            IDLE_THREAD();
            if(idling && ::tick_count()-idldetick > 3000)
            {
                Sqrat::SharedPtr<int> srv;

                idldetick = ::tick_count();
                idling = true;

                if(lastMsg.ptr())
                {
                    LOGDETAIL2("calling usr callback type=IDLING. Callback should handle IDLING types too.");
                    thismsg = "cust ";
                    srv = lastMsg->_func.Evaluate<int>(lastMsg->_pc, IDevice::eIDLE, ::tick_count()-startt);
                    if(srv.Get()==0)
                    {
                        throw (Sqrat::Exception(" Callback or loop function must return a value "));
                    }
                    else
                        rv = (*srv.Get());

                    ++lastMsgPoke;
                    if(lastMsgPoke>10)
                    {
                        LOGWARN("Script stuck in last user callback. Did you return -1...");
                        lastMsgPoke=0;
                    }
                }
                else if(_funcloop.IsNull())
                {
                    LOGDETAIL2("loop() not found to call it for IDLING...");
                }
                else
                {
                    srv = _funcloop.Evaluate<int>(0,IDevice::eIDLE, ::tick_count()-startt);
                    if(srv.Get()==0)
                    {
                        throw (Sqrat::Exception(" Callback or loop function must return a value "));
                    }
                    else
                        rv = (*srv.Get());
                    thismsg = "idle ";
                }
            }
        }
        if(_Debug && !RunCtx::_ShellOn && counter % 8==0)
        {
            std::string threads;

            std::map<std::string, std::string>::iterator f = _threads.begin();
            for(; f!= _threads.end();f++){
                threads += f->first;
                threads += f->second;
                threads += " ";
            }
            printf("%s%s%c\r", thismsg.c_str(),threads.c_str(), round[counter%5]);
            std::cout<<std::flush;
            std::cout<<std::flush;
        }

        ++counter;
    }while(rv!=-1 && _Alive && ::msleep(interval) && loop);
    if(loop)
        RunCtx::_looPing = false;

    return rv;
}

/**
 * @brief RunCtx::callScript
 * @param who
 * @param err
 * @param msg
 */
void RunCtx::callScript(const Device* who, int err, const char* msg)
{
    Autoreset a(&RunCtx::_InScript);

    Sqrat::Function f = Sqrat::RootTable().GetFunction(_SC("loop"));
    if(f.IsNull()){
        LOGERR("Please define function 'loop(dev, msgid, data);' in your script");
        return;
    }
    try{
        if(who->useptr())
            f.Evaluate<int>(who, err, msg);
        else
            f.Evaluate<int>(who->name(), err, msg);
    }
    catch(Sqrat::Exception ex)
    {
        LOGERR(ex.Message());
        RunCtx::_Alive = false;
    }
}

/**
 * @brief RunCtx::addThreadSpin
 * @param name
 * @param dir
 * @param lop
 */
void RunCtx::addThreadSpin(std::string& name, const char* dir, int lop)
{
    if(lop==-1)
    {
        std::map<std::string, std::string>::iterator f = _threads.find(name);
        if(f != _threads.end())
            _threads.erase(f);
        return;
    }
    char data[32];
    ::sprintf(data, "%s%d", dir,lop);
    _threads[name] = data;
}

/**
 * @brief RunCtx::callSqFoo
 * @param dev
 * @param f
 * @param msg
 * @return
 */
int RunCtx::callSqFoo(const Device* dev, Sqrat::Function& f, const CntPtr<DevMsg>& msg)
{
    int                   rv = 0;
    Autoreset             a(&RunCtx::_InScript);
    Sqrat::SharedPtr<int> srv;

    if(msg->_msg == IDevice::eBINARY)
    {
        Autoreset    a(&RunCtx::_InScript);
        Sqrat::Array ra(_Penv->theVM(), msg->_data.length());

        LOGREC( toString(msg->_data.data(), msg->_data.length()));

        cbyte* buffer = (cbyte*)msg->_data.data();
        for(size_t idx = 0 ; idx < msg->_data.length(); idx++)
        {
            ra.SetValue(idx, uint8_t(buffer[idx]));
        }
        LOGDETAIL2("Calling script loop/user");
        if(dev==0)//called from Contrext
        {
            srv = f.Evaluate<int>(0, msg->_msg, ra);
        }
        else if(dev->useptr())
            srv = f.Evaluate<int>(dev, msg->_msg, ra);
        else
            srv = f.Evaluate<int>(dev->name(), msg->_msg, ra);
        if(srv.Get()==0)
        {
            throw (Sqrat::Exception(" Callback or loop(...) function must return a value "));

        }
        else
            rv = (*srv.Get());
    }
    else if(msg->_msg == IDevice::eTEXT || msg->_msg == IDevice::eERROR)
    {
        std::string chunk = (const char*)msg->_data.c_str();
        LOGREC( chunk );
        if(dev==0)//called from Contrext
        {
            srv = f.Evaluate<int>(0, msg->_msg, chunk.c_str());
        }
        else if(dev->useptr())
            srv = f.Evaluate<int>(dev, msg->_msg, chunk.c_str());
        else
            srv = f.Evaluate<int>(dev->name(), msg->_msg, chunk.c_str());

        if(srv.Get()==0)
        {
            throw (Sqrat::Exception(" Callback or loop function must return a value "));
        }
        else
            rv = (*srv.Get());
    }
    return rv;
}

const char* RunCtx::System(const char* cmd)
{
    static std::string result;
#ifdef __linux
    FILE* pipe = popen(cmd, "r");
    if (pipe)
    {
        char buffer[128];
        try
        {
            while (!::feof(pipe))
            {
                size_t bytes = fread(buffer,1, sizeof(buffer)-1, pipe);
                if (bytes > 0)
                {
                    buffer[bytes] = 0;
                    result += buffer;
                }
            }
        }
        catch(Sqrat::Exception ex)
        {
            LOGERR(ex.Message());
            RunCtx::_Alive = false;
            throw ex;
        }
        catch (...)
        {
            LOGERR("Exception:" << cmd << ", err:" << errno);
        }
        ::pclose(pipe);
    }
    else
    {
        LOGERR("Cannot open:" << cmd << ", err:" << errno);
    }
#endif
    return result.c_str();
}

int RunCtx::SystemCb(const char* cmd, Sqrat::Function f)
{
#ifdef __linux
    FILE* pipe = popen(cmd, "r");
    if (pipe)
    {
        uint8_t buffer[128];
        try
        {
            CntPtr<DevMsg> msg(new DevMsg(0, IDevice::eTEXT, 0, 0));
            while (!::feof(pipe))
            {
                size_t bytes = fread(buffer,1, sizeof(buffer)-1, pipe);
                if (bytes > 0)
                {
                    buffer[bytes] = 0;
                    msg->setData(buffer,bytes);
                    RunCtx::callSqFoo(0, f, msg);
                }
            }
        }
        catch(Sqrat::Exception ex)
        {
            LOGERR(ex.Message());
            RunCtx::_Alive = false;
            throw ex;
        }
        catch (...)
        {
            LOGERR("Exception:" << cmd << ", err:" << errno);
        }
        ::pclose(pipe);
        return 0;
    }
    LOGERR("Cannot open:" << cmd << ", err:" << errno);
#endif
    return -1;
}

/**
 * @brief RunCtx::Error
 * @param msg
 * @return
 */
int RunCtx::Error(const char* msg)
{
    std::cout << TC_KRED << msg << TC_WHITE << std::endl;
    return 0;
}

/**
 * @brief RunCtx::WaitDevices
 * @param to
 * @return
 */
int RunCtx::WaitDevices(int to)
{
    size_t fut = ::tick_count() + to;

    size_t devs;
    std::set<Device*>::iterator it;

    while(tick_count() < fut && devs)
    {
        devs = _Devices.size();
        it =_Devices.begin();
        for(; it != _Devices.end(); ++it)
        {
            Device* pc = *it;
            if(pc->isopen())
                devs--;
        }
        ::msleep(1024);
        LOGDETAIL2("waiting for devs to be open");
    }
    return devs;
}

int RunCtx::Looping()
{
    RunCtx::_looPing=true;
    return 0;
}

int RunCtx::EnableLog(const char* logfile, int max, int many)
{
    _LogFile = logfile;
    _LogFile += "_0";

    FILE* pf = fopen(_LogFile.c_str(),"ab");
    if(pf == 0)
        pf = fopen(_LogFile.c_str(),"wb");
    if(pf)
    {
        _LogFiles = many;
        _LogFileSz = max;
        ::fclose(pf);
        return 0;
    }
    _LogFile.clear();
    return LASTR_ERR();
}

int RunCtx::Log(const char* logstr)
{
    if(!_LogFile.empty())
    {
        FILE*   pf = fopen(_LogFile.c_str(),"ab");
        if(pf)
        {
            RunCtx::_LogFileLastSz = ::ftell(pf);
            ::fprintf(pf,"%s: %s\r\n", str_time(), logstr);
            ::fclose(pf);
        }
        _LogManage();
        return 0;
    }
    RunCtx::_LogFileLastSz = 0;
    //GLOGW("Log file has not been configured");
    return -1;
}

int RunCtx::_LogManage()
{
    if(!_LogFile.empty())
    {
        char fp[1024];
        char fps[1024];
        char dir[512] = {0};
        char fname[256] = {0};


        ::strcpy(fp,  _LogFile.c_str());
#ifdef __linux
        ::strcpy(fname, _LogFile.substr(_LogFile.find_last_of('/')).c_str());
        ::strcpy(dir, ::dirname(fp));

#else
        char drv[32] = {0};
        char ext[256] = {0};
        //_splitpath( fp, drv, dir, fname, ext);
#endif
        if(::strchr(fname,'_')!=0)
            *(::strchr(fname,'_'))=0;

        if(RunCtx::_LogFileLastSz > _LogFileSz)
        {
            for(int k=_LogFiles; k>0; k-- )
            {
    #ifdef __linux
                ::sprintf(fps,"%s%s_%d",dir,fname,k);
                ::sprintf(fp,"%s%s_%d",dir,fname,k-1);
                if(k == _LogFiles)
                {
                    ::unlink(fps);
                }
                else
                {
                    ::rename(fp, fps);
                }
    #else
                ::sprintf(fps,"%s:%s/%s.%s_%d",drv,dir,fname,ext,k);
                ::sprintf(fp,"%s:%s/%s.%s_%d",drv,dir,fname,ext,k-1);
                if(k == _LogFiles)
                {
                    DeleteFile(fps);
                }
                else
                {
                    MoveFile(fp, fps);
                }
    #endif
            }
            FILE* pf = ::fopen(_LogFile.c_str(),"wb");
            if(pf)
                ::fclose(pf);
        }
    }
    return 0;
}

/*
void RunCtx::Plug(const char* sub)
{
    std::string fullfile;
#ifdef __linux
    fullfile = "lib";
    fullfile += sub;
#ifdef DEBUG
    fullfile += "D";
#endif
    fullfile += ".so";
    SoHandler h = ::dlopen(fullfile.c_str(), RTLD_LAZY);
    if(h==0)
    {
        fullfile = "lib";
        fullfile += sub;
        fullfile += ".so";
        h = ::dlopen(fullfile.c_str(), RTLD_LAZY);
    }
#else
    fullfile += sub;
#ifdef DEBUG
    fullfile += "D";
#endif
    fullfile += ".dll";
    SoHandler h = LoadLibrary(fullfile.c_str());
#endif
    if(h)
    {
        _plugin = new SoEntry(h);
        return _plugin->_so_get(PCTX, 0, sub, PSq->theVM());
    }
    else
    {
#ifdef __linux
        LOGERR("Cannot load:" << fullfile << ": " <<::dlerror());
#else
        LOGERR("Cannot load:" << fullfile << ": "<< GetLastError());
#endif
    }
    return 0;
}
*/

