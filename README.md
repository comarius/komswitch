## Scripted terminal for Linux and Windoze... docklight like
Scripting like java program language to create and run automated test jobs.  
Suports: (RS232, RS485/422), TCP or UDP, USB, SSH


[script engine linux] ----> [device],[device],[many devices]


### The scripted engine uses Squirrel script, due it's elegance and easy 'SqRat' C++ binding.

Language global functions:
    All squirrel functionss and types https://electricimp.com/docs/squirrel/




```javascript

// a sample showing the process status from local  machine and a remote Linux connected over serial and ssh

function main(xxx)
{
    //
    // shows proc status in a variable
    //
    var ps = System("ps -ax");
    print(ps);
    
    //
    // shows proc ststus in a callback
    //
    System("ps -ax", findproc);


    var ser = Device("tty:///dev/ttyUSB0,115200,8N1", null, TEXT);
    var ssh = Device("ssh://pi/raspberry@192.168.1.112:22", "a ssh", TEXT);
    var usb = Device("susb://0x402:0x5007", "usb", BINARY);
    
    ser.putsnlcb("",signon);
    ser.putsnlexpect("","pi");    
    ser.putsnl("ps -ax");
    ssh.print(4000);        // prints putput data. waits for it at least 4 seconds,
                            // if new data comes it would extend the timeout for another 4 seconds.
                            // would time out when there is not data received for at leat 4 seconds
    
    ssh.putsnlexpect("","pi");     // sends cr and expets a prompt containing the pi substring
    ssh.putsnl("ps -ax");
    ssh.print(4000);    
    usb.write([1,2,3,4,5,6,7,8,9,0]);
    return Run();   
}

function loop(wnat, sg, data)
{
     if(sg==BINARY){
         ArrDbg(data,0);
     }
     return 0;      
}

function signon(dev, msg, data)
{
    if(msg==IDLE)
        return -1;
    if(data.find("password")!=null)
    {
       dev.putsnl("raspberry");
       return -1;
    }

    if(data.find("username")!=null) 
    {
       dev.putsnl("pi");
       return 1;
    }
    return 0;
}

function findproc(dev, msg, data)
{
    //
    // dev is 0 on global cb functions
    //
    print(data);
    if(data.find("komsw") != null)
    {
        print("found komswi program running \n");
    }
    return 1; //  let the system clear the data
}

```

## Ugly inacurate Help, see source code for reference.

```console

-------------------------------------------------------------------
|                      API's                                      |
-------------------------------------------------------------------
GLOBAL CONSTANTS
    Data types
    Message Id's, loop second parameter
         CLOSED Message id in loop(), A device is closed,
         IDLING Message id in loop(), Process idling
         OPENED Message id in loop(), A device is opened
         BINARY Message id in loop(), Data is binary
         TEXT Message id in loop(), Data is text
         RECEIVING message id in loop(), Device is receiveing. For file transfer
         SENDING message id in loop(), Device is sending. For file transfer

    Return codes from loop
         PROCESSED Return it from loop() if you read/processed the data, so internal cache coul dbe deleted
         CONTINUE Return it from loop() to keep the script Run(), running, and received dat to be hold for as much next loop so it can be read with read/gets device methods
         EXIT Return it from loop() to terminate the script

    timeout predefined values for Sleep/wait/read/gets Api's
         AYEAR a year timeout
         ADAY a dey timeut
         ANHOUR an hour timeout

    Predefined key codes to send with Device.puts() API
         F1 key F1
         UP key
         DOWN  key
         LEFT
         RIGHT
         DEL
         INS
         END
         HOME
         ENTER
         RETURN

    Global functions 
         void Run() Runs the loop() function if scrpts defines it.
         void Debug(1..3) Sets debug level 0,1,2,3
         out System(cmd) Runs a command shell, and returns i'ts output
         SystemCb(cmd, cb) runs a commad in shell and for each output of the command line/chunk calls cb() function
         Exit(ms) Exits after ms milliseconds. 
         Sleep(ms) Sleeps ms milliseconds
         int Str2num(string) Converts "0x12" -> 18
         $ Arr2str([]) Converts ['A','B'] - > "AB" assumed elements are bytes
         [] Str2arr($)  Converts"0203" -> [2,3]
         $ Num2str(#,#) Converts  (15,1) -> "0x0F"   15,2 -> "0x000F", Up to 8 bytes
         [] Num2arr(#) As the above function but erturns an array of bytes. Up to 8 bytes
         $ Arr2ascii([])  Converts [] -> ASCII. Non printable are replaced with '.'
         # Arr2rint([],1..8) Converts  builds a uint8_t up to uint64_t from bytes, Big Endian
         # Arr2int([],1..8) Converts  builds a uint8_t up to uint64_t from bytes, Little Endian
         $ SubArr2str([],start,count) Converts  as Ass2Str form index from, count elements, assumed elements are bytes
         # ArrFind([haystack],[needle]) Converts  Search needle. Returns the index of match, or -1 if no match
         # ArrEqual([],[]) Converts Retuens memcmp() between contents, 0 if arrays are equal
         Shell() Opens a text raw shell to device. Messages are not routed
         Device Dev($)  Returns device by name passed int he device constructor
         Cin()  Expects th auser to type some input. The input is returned to script
         Help() Prints this help

    Device Object methods
    Device($cs, $name, MODE) Creates a device object, 
                  $cs - connection string is: 'protocol://specific_to_device' 
                     protcol:  tty, phydget, file  (To add: 'ssh,curl,sftp,qtui,https/http'')
                       OK  tty         Serial device RS232 tty://COM1,115200,8N1
                       OK  phy         phydge USB device phydget://SERIAL,I/O
                       OK  file        Regular disk file file://C:\myfile.txt
                       OK  sftp/ssh    ssh and sftp ssh://<user/<password>@>IP<:PORT>
                       OK  http/https  http protocols http<s>://address
                       TBD qtui        user interface (TO SEE) qtui://qmlfile.qml
                  $name - a stirng to id the device used to get it any time with Dev($) global function.
                           if string is null, then loop() function would get as first parameter the device instance instead device name
                  MODE   = TEXT or BINARY
All functions with return type '#' (number) return 0 for success. Otherwise last system error code.
All functions with return type '$' can return null or an empty string
All functions with return type '[]' can return null or an empty array
         seteol($) Sets the end of line when putsnl() is called. Default eol is "\r\n" 
         putsnl($) Sends text $ to device and appends EOL set up before.  
         puts($)  Sends test as is to device. Except custom devices
         putsml($) Sends multiline text to device 
         write([]) Sends the array of bytes 
         # reconnect(#)  reconnects back to deivce. # is timeout. Returns 0 success, -1 no device available, 1- not connected/timeout
         setbuff(#bytes) Sets the maxim buffer to keep received data. Max 16000 bytes, Default 512
         $ gets()  Return received string, if any.
         $ gets(#to) With timeout, where you see #to, is timeout in mseconds 
         [] read() Read bytes from device, if any 
         [] read(#to)  
         readmin(#bytes)  Waits to get from device at least # bytes 
         readmin(#bytes,#to) Same as above but with timeout 
         issteady(#msecs)  If the device does not produce any output for al least #for ms.
         true/false isopen()  Returns true if device is open
         open($constring)  TO DO !!!
         flushin()  clears received data
          # strexpect($exp, #to) Expects in incoming data to see the 'exp', for timeout. Returns 0 if found.
           # strexpect($exp)  
           # binexpect([], #to)  
           # binexpect([])  
           # bytesexpect(#minbytes)  
           # bytesexpect(#minbytes)  
           # expectany() Expects any data from device 
           # expectany(#to)  Expects any data from device with custom timeout
          [] writeread([]) Write,Reads in one shot. 
           [] writeread([],to) Write,Reads in one shot, with timeout
           $ putsgets($)   Puts,gets in one shot.
           $ putsgets($, #to)   Puts,gets in one shot with timeout
           $ putsnlgets($)   Puts and appends sendcr<+LF> ,gets in one shot.
           $ putsnlgets($, #to)   Puts and appends sendcr<+LF> gets in one shot with timeout
          # writeexpect([],[]) Write and expects. 
           # writeexpect([],[],#to) Write and expect with timeouts. 
           # putsexpect($w,$e)  Same as above with strings
           # putsexpect($w,$e)  
           # putsnlexpect($w,$e)  Same as above with new line
           # putsnlexpect($w,$e,#to)  
         flush()  Cleans slate, all i/o are cleared.
         expectprompt(#to) Waits for any output, then waits for steady for as long to milliseconds
         setspeed(3ms) Bytes apart in milliscond, when sending them to device. Default is 0
         setoptions($) Custom to plugins if any specific  
         settimeout(#to) Sets default timeout for write/read, when used without specific timeout parameter. Defualt 600ms 
         putctrl('$') Likewise putctrl('C'), sends Control+C
         close() Closes the device. TODO
         cbputs($, cb) Puts a string, then for any output calls the cb function
         cbputsnl($, cb) Puts a string+CRLF, then for any output calls the cb function
         cbputml($), cb Puts a multiline string, then for any output calls the cb function
         cbwrite([], cb) Writes the array then for any output calls the cb function
         crexpect($, #) Sends Enter to device, then expects $ for # timeout
         crexpect($), cb Sends Enter to device, then expects $ for default timeout
         cr() Sends Enter to device
 Procedures/Callbacks
      int loop(dev, message, data) Loop function would be called if is present, if main ends with Run()
              Called to indicate an event of a specific device.
              return 1 to discard received data, 0 to hold it and be available  by a device read/gets, -1 to exit the script.
              dev - would be a string, with device name if device constructor had a non empty name
              message - CLOSED,IDLING,OPENED,BINARY,TEXT,ERCEIVING,SENDING
              data - data associated with the message
      int cb(dev, message, data) callback function signature in all cb calls
              Called to indicate all events on the device the cb call was done.
              Applies to cbputs, cbputsnl cbputml cbwrite.
```



Need, help shoot in the issues tab.

