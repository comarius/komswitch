#ifdef __linux
#include <unistd.h>
#else
#include <windows.h>
#endif

#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <iterator>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include "phydget.h"

/*
 *  phy://222RW,ID
*/

static void CCONV onAttachHandler(PhidgetHandle phid, void *ctx);
static void CCONV onDetachHandler(PhidgetHandle phid, void *ctx);
static void CCONV errorHandler(PhidgetHandle phid, void *ctx, Phidget_ErrorEventCode errorCode, const char *errorString);

static void CCONV onStateChangeHandler0(PhidgetDigitalInputHandle ch, void *ctx, int state);
static void CCONV onStateChangeHandler1(PhidgetDigitalInputHandle ch, void *ctx, int state);

static PhidgetReturnCode CCONV _initChannel(PhidgetHandle ch, int channel, Phydget* ctx) ;
static Phydget* _Phidget;
/**
 * @brief Phydget::Phydget
 * @param pdev
 * @param constr
 * @param name
 */

class WordDelimitedByCommas : public std::string{};
std::istream& operator>>(std::istream& is, WordDelimitedByCommas& output)
{
   std::getline(is, output, ',');
   return is;
}

Phydget::Phydget(IDevice* pdev,  const char* constr, const char* name):_pdev(pdev),
                                                                      _constr(constr),
                                                                      _debug(0),
                                                                      _name(name),
                                                                      _attached(false)
{
    _chIa = 0;
    _chIb = 0;
    _chOa = 0;
    _chOb = 0;

    pdev->addDev(this);
    _Phidget = this;
    reopen(constr);
}

Phydget::~Phydget()
{
    _pdev->remDev(this);
}

int Phydget::reopen(const char* where)
{
    if(_constr.find(',') == std::string::npos)
    {
        _pdev->devEvent(this, IDevice::eERROR, (cbyte*)"Phy string should be: phy://SERIAL,I/O",0);
        return 2;
    }

    std::istringstream iss(_constr);
    std::vector<std::string> results((std::istream_iterator<WordDelimitedByCommas>(iss)),
                                      std::istream_iterator<WordDelimitedByCommas>());
    if(results.size()!=2)
    {
        _pdev->devEvent(this, IDevice::eERROR, (cbyte*)"Phy string should be: phy://SERIAL,I/O",0);
        return 3;
    }
    _serial = 0;
    _serial = ::atoi(results[0].c_str());
    _rw = results[1];
    return 0;
}

const char* Phydget::name()const
{
    return _name.c_str();
}

int Phydget::open()
{
    PhidgetReturnCode res=EPHIDGET_INVALIDARG;
    const char *errs;
    std::string err;


    if(_rw.empty())
    {
        if(_debug)
            fprintf(stderr, "Phy string should be: phy://SERIAL,I/O\n");
        return 2;
    }
    close();

    if(_rw.find("O") != std::string::npos)
    {
        res = PhidgetDigitalOutput_create(&_chOa);  if (res != EPHIDGET_OK) {err = "create 0a"; goto ERR;};
        res = _initChannel((PhidgetHandle)_chOa, 0, this); if (res != EPHIDGET_OK) {err = "init 0a"; goto ERR;};
        res = Phidget_openWaitForAttachment((PhidgetHandle)_chOa, 2000); if (res != EPHIDGET_OK) {err = "attach 0a"; goto ERR;};

        res = PhidgetDigitalOutput_create(&_chOb);  if (res != EPHIDGET_OK) {err = "create 0b"; goto ERR;};
        res = _initChannel((PhidgetHandle)_chOb, 1, this); if (res != EPHIDGET_OK) {err = "init 0b"; goto ERR;};
        res = Phidget_openWaitForAttachment((PhidgetHandle)_chOb, 2000); if (res != EPHIDGET_OK) {err = "attach 0b"; goto ERR;};
    }
    if(_rw.find("I") != std::string::npos)
    {
        res = PhidgetDigitalInput_create(&_chIa);if (res != EPHIDGET_OK) {err = "create Ia"; goto ERR;};
        res = _initChannel((PhidgetHandle)_chIa, 0, this);if (res != EPHIDGET_OK) {err = "init Ia"; goto ERR;};
        res = PhidgetDigitalInput_setOnStateChangeHandler(_chIa, onStateChangeHandler0, this);if (res != EPHIDGET_OK) {err = "state Ia"; goto ERR;};
        res = Phidget_openWaitForAttachment((PhidgetHandle)_chIa, 2000);if (res != EPHIDGET_OK) {err = "create Ia"; goto ERR;};

        res = PhidgetDigitalInput_create(&_chIb);if (res != EPHIDGET_OK) {err = "create Ib"; goto ERR;};
        res = _initChannel((PhidgetHandle)_chIb, 1, this);if (res != EPHIDGET_OK) {err = "init Ib"; goto ERR;};
        res = PhidgetDigitalInput_setOnStateChangeHandler(_chIb, onStateChangeHandler1, this);if (res != EPHIDGET_OK) {err = "state Ib"; goto ERR;};
        res = Phidget_openWaitForAttachment((PhidgetHandle)_chIb, 2000);if (res != EPHIDGET_OK) {err = "create Ib"; goto ERR;};
    }
    return res;
ERR:
    Phidget_getErrorDescription(res, &errs);
    fprintf(stderr, "Err desc: %s, %s  err: %d\n", errs, err.c_str(), err);
    close();
    return res;
}

int Phydget::close()
{
    if(_chIa)
    {
        Phidget_close((PhidgetHandle)_chIa);
        PhidgetDigitalInput_delete(&_chIa);
    }
    if(_chIb)
    {
        Phidget_close((PhidgetHandle)_chIb);
        PhidgetDigitalInput_delete(&_chIb);
    }
    if(_chOa)
    {
        Phidget_close((PhidgetHandle)_chOa);
        PhidgetDigitalOutput_delete(&_chOa);
    }
    if(_chOb)
    {
        Phidget_close((PhidgetHandle)_chOb);
        PhidgetDigitalOutput_delete(&_chOb);
    }
    _chIa = 0;
    _chIb = 0;
    _chOa = 0;
    _chOb = 0;

    return 0;
}

void Phydget::setspeed(int ms)
{
}

//  SLHCW|R:555
int Phydget::setOptions(const char* op)
{
    return 0;
}

int Phydget::sendTo(cbyte* data, int len)
{
    char local[16];
    ::strcpy(local, (const char*)data);
    if(::strchr(local,','))
    {
        int channel = ::atoi(::strtok(local,","));
        int dt = ::atoi(::strtok(0,","));
        if(_debug==2)
            printf("   port sends digital data %d to channel %d\n",dt, channel);
        if(channel==0)
            return PhidgetDigitalOutput_setState(_chOa, dt);
        else
            return PhidgetDigitalOutput_setState(_chOb, dt);
    }
    int dig;
    ::sscanf((const char*)data,"%d",&dig);
    if(_debug==2)
        printf("   port sends digital data %d \n",dig);
    int res = PhidgetDigitalOutput_setState(_chOa, dig);
    res |= PhidgetDigitalOutput_setState(_chOb, dig);
    return res;
}

int Phydget::recFrom(unsigned char* bytes, int room)
{
    if(_changed)
    {
        _changed = false;
        size_t len = _lastdata.length();
        if(len)
            ::strcpy((char*)bytes, _lastdata.c_str());
        _lastdata.clear();
        return int(len);
    }
    return 0;
}

void Phydget::debug(int d)
{
    _debug = d;
}

void Phydget::setTout(int to)
{
}

bool Phydget::isOpen()
{
    return (_chOa || _chOb || _chIa || _chIb) != 0;
}

void Phydget::flush()
{
}

int Phydget::copyFile(const char* fileS, const char* fileD, bool)
{
    return 1;
}

IDevice::D_MSG Phydget::getMode()
{
    return IDevice::eTEXT;
}


int Phydget::esc(const char*, int)
{
	return -1;
}

int Phydget::removeFile()
{
	return -1;
}

int Phydget::createFile()
{
	return -1;
}

IDevice::D_MSG Phydget::getMode()const
{
	return IDevice::eTEXT;
}

static void CCONV onAttachHandler(PhidgetHandle phid, void *ctx)
{
    Phydget* p = static_cast<Phydget*>(ctx);
    PhidgetReturnCode res;
    int hubPort;
    int channel;
    int serial;

    p->_changed = true;
    res = Phidget_getDeviceSerialNumber(phid, &serial);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to get device serial number\n");
        return;
    }

    res = Phidget_getChannel(phid, &channel);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to get channel number\n");
        return;
    }

    res = Phidget_getHubPort(phid, &hubPort);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to get hub port\n");
        hubPort = -1;
    }

    if(_Phidget->_debug)
    {
        if (hubPort == -1)
        {
            printf("    channel %d on device %d attached\n", channel, serial);
        }
        else
        {
            printf("    channel %d on device %d hub port %d attached\n", channel, serial, hubPort);
        }
    }
    p->_attached = true;
}

static void CCONV onDetachHandler(PhidgetHandle phid, void *ctx)
{
    Phydget* p = static_cast<Phydget*>(ctx);
    PhidgetReturnCode res;
    int hubPort;
    int channel;
    int serial;

    p->_changed = true;
    res = Phidget_getDeviceSerialNumber(phid, &serial);
    if (res != EPHIDGET_OK) {
        if(_Phidget->_debug)
        fprintf(stderr, "failed to get device serial number\n");
        return;
    }

    res = Phidget_getChannel(phid, &channel);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to get channel number\n");
        return;
    }

    res = Phidget_getHubPort(phid, &hubPort);
    if (res != EPHIDGET_OK)
        hubPort = -1;

    if(_Phidget->_debug)
    {
    if (hubPort != -1)
        printf("    channel %d on device %d detached\n", channel, serial);
    else
        printf("    channel %d on device %d hub port %d detached\n", channel, hubPort, serial);
    }

    p->_attached = false;
}

void CCONV errorHandler(PhidgetHandle phid, void *ctx, Phidget_ErrorEventCode errorCode, const char *errorString)
{

    Phydget* p = static_cast<Phydget*>(ctx);
    if(_Phidget->_debug && errorCode != EEPHIDGET_PACKETLOST)
    {
        fprintf(stderr, "Error: %s (%d)\n", errorString, errorCode);
        p->_pdev->devEvent(p, IDevice::eERROR, (cbyte*)errorString, ::strlen(errorString));
    }
    p->_changed = true;
}

void CCONV onStateChangeHandler0(PhidgetDigitalInputHandle ch, void *ctx, int state)
{
    Phydget* p = static_cast<Phydget*>(ctx);
    char out[16] = {0};
    int channel = -1;
    Phidget_getChannel((PhidgetHandle)ch, &channel);
    ::sprintf(out,"0,%d",state);
    p->_lastdata = out;
    if(_Phidget->_debug)
        printf(" port state changed: %s", out);
    p->_changed = true;
    p->_pdev->devEvent(p, IDevice::eTEXT, (cbyte*)out, ::strlen(out));
}

void CCONV onStateChangeHandler1(PhidgetDigitalInputHandle ch, void *ctx, int state)
{
    Phydget* p = static_cast<Phydget*>(ctx);
    char out[16] = {0};
    ::sprintf(out,"1,%d",state);
    p->_lastdata = out;
    if(_Phidget->_debug)
        printf(" port state changed: %s", out);
    p->_changed = true;
    p->_pdev->devEvent(p, IDevice::eTEXT, (cbyte*)out, ::strlen(out));
}

/*
* Creates and initializes the channel.
*/
PhidgetReturnCode CCONV _initChannel(PhidgetHandle ch, int channel, Phydget* ctx)
{
    PhidgetReturnCode res = (PhidgetReturnCode)0;
    res = Phidget_setDeviceSerialNumber(ch, ctx->_serial);

    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to set label/serial\n");
        return (res);
    }
    res = Phidget_setChannel(ch, channel);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to set channel %d\n", channel);
        return (res);
    }
    res = Phidget_setOnAttachHandler(ch, onAttachHandler, ctx);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to assign on attach handler\n");
        return (res);
    }
    res = Phidget_setOnDetachHandler(ch, onDetachHandler, ctx);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to assign on detach handler\n");
        return (res);
    }

    res = Phidget_setOnErrorHandler(ch, errorHandler, ctx);
    if (res != EPHIDGET_OK) {
        fprintf(stderr, "failed to assign on error handler\n");
        return (res);
    }
    ctx->_changed=true;
    return (EPHIDGET_OK);
}


/**
 *
 */
extern "C"
{
    EXPORT_CLS  IComm* getInstance(IDevice* pd, const char* cs, const char* name)
    {
        return new Phydget(pd, cs, name);
    }

    EXPORT_CLS  void destroyInstance(IComm* pc)
    {
        delete pc;
    }
}


