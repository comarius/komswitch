#-------------------------------------------------
#
# Project created by QtCreator 2017-10-17T16:31:38
#
#-------------------------------------------------

QT       -= core gui

TARGET = phydget
TEMPLATE = lib

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O0 -Wno-unused-parameter

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG += -O0 -Wno-unused-parameter

INCLUDEPATH += ../../
INCLUDEPATH += ./deplibs/linux/libphidget22-1.0.0.20170906/ROOTFS/include
QMAKE_LFLAGS += -fPIC
DEFINES += PHYDGET_LIBRARY
DEFINES += SO_LIBRARY

SOURCES += phydget.cpp
HEADERS += phydget.h\
        phydget_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}


unix|win32: LIBS += -lphidget22
