#-------------------------------------------------
#
# Project created by QtCreator 2017-10-17T16:31:38
#
#-------------------------------------------------

QT       -= core gui

TARGET = file
TEMPLATE = lib

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O0 -Wno-unused-parameter

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG += -O0 -Wno-unused-parameter

INCLUDEPATH += ../../
INCLUDEPATH += ../../sq/include
INCLUDEPATH +=../../sq/sqrat/include/

QMAKE_LFLAGS += -fPIC
DEFINES += PHYDGET_LIBRARY
DEFINES += SO_LIBRARY

SOURCES += pfile.cpp
HEADERS += pfile.h

target.path = ../../bin/plugins
INSTALLS += target

