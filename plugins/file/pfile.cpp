
#ifdef __linux
#   include <unistd.h>
#   include <errno.h>
#else
#   include <windows.h>
#endif

#include "pfile.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "squirrel.h"
#include "sqrat.h"

HSQUIRRELVM VM;

PFile::PFile(IDevice* pdev,  const char* constr, const char* name):_debug(0),
                                                                    _fname(constr),
                                                                    _open(false),
                                                                    _name(name)


{
    _pdev = pdev;
    pdev->addDev(this);
}

PFile::~PFile()
{
    _pdev->remDev(this);
}

const char* PFile::name()const
{
    return _name.c_str();
}

int PFile::reopen(const char* cs)
{
    _fname = cs;
    return 0;
}

int PFile::open()
{
    _open=true;
    return 0;
}

int PFile::close()
{
    _open=false;
    return 0;
}

void PFile::setspeed(int ms)
{
}

int PFile::sendTo(const uint8_t* data, int len)
{
    size_t r = 0;

    if(_debug)
        printf(" writing to file %s bytes\n", _fname.c_str());

    FILE* pf = fopen(_fname.c_str(),"wb");
    if(pf)
    {
        r = ::fwrite(data, 1, len, pf);
        if(_debug)
            printf("...port wrote in file %d bytes\n", int(r));
         ::fclose(pf);
    }
    _prevdata.clear();
    return r==size_t(len) ? 0 : 1;
}

int PFile::recFrom(unsigned char* bytes, int room)
{
    size_t r = 0;

    FILE* pf = fopen(_fname.c_str(),"rb");
    if(pf)
    {
        r = ::fread(bytes, 1, room, pf);
        ::fclose(pf);
    }
    if(_prevdata!=(const char*)bytes && r)
    {
        _prevdata = (const char*)bytes;
        return r;
    }
    _prevdata.clear();
    return 0;
}

void PFile::debug(int d)
{
    _debug = d;
}

void PFile::setTout(int to, int selt)
{
}

bool PFile::isOpen()
{
    return _open;
}
void PFile::flush()
{

}

int PFile::setOptions(const char*)
{
    return 0;
}


int PFile::copyFile(const char* fileS, const char* fileD, bool)
{
    return 1;
}

IDevice::D_MSG PFile::getMode()const
{
    return IDevice::eTEXT;
}

int PFile::esc(const char*,int)
{
    return -1;
}

int PFile::removeFile()
{
    if(_debug)
        printf(" file %s deleted\n", _fname.c_str());
#ifdef __linux
    unlink(_fname.c_str());
#else
    DeleteFile(_fname.c_str());

#endif
    _prevdata.clear();
    return 0;
}

int PFile::createFile()
{
    FILE* pf = fopen(_fname.c_str(),"wb");
    if(pf){
        fclose(pf);
        return 0;
    }
#ifdef __linux
    return errno;
#else
    return GetLastError();
#endif
}

void PFile::testin(const char* param)
{
    std::cout << param << "\n";
}

void PFile::publish()
{
    std::cout << "publishing \n";

    Sqrat::Class<PFile> cls(VM, _SC("file"));
    cls.Func(_SC("textin"), &PFile::testin);

    Sqrat::RootTable().Bind(_SC("file"), cls);
}

/**
 *
 */
extern "C"
{
    EXPORT_CLS  IComm* getInstance(IDevice* pd, const char* cs,
                                   const char* name, HSQUIRRELVM vm)
    {
        VM = vm;
        return new PFile(pd, cs, name);
    }

    EXPORT_CLS  void destroyInstance(IComm* pc)
    {
        delete pc;
    }
}

