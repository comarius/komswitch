#-------------------------------------------------
#
# Project created by QtCreator 2017-10-17T16:31:38
#
#-------------------------------------------------

QT       -= core gui

TARGET = file
TEMPLATE = lib

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O0 -Wno-unused-parameter

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG += -O0 -Wno-unused-parameter

INCLUDEPATH += ../../
QMAKE_LFLAGS += -fPIC
DEFINES += PHYDGET_LIBRARY
DEFINES += SO_LIBRARY

SOURCES += pfile.cpp
HEADERS += pfile.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}


