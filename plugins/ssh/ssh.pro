#-------------------------------------------------
#
# Project created by QtCreator 2017-10-12T13:12:29
#
#-------------------------------------------------

QT       -= core gui

TARGET = ssh
TEMPLATE = lib


QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O0 -Wno-unused-parameter

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG += -O0 -Wno-unused-parameter

INCLUDEPATH += ../../
INCLUDEPATH += ../../sq/include
INCLUDEPATH +=../../sq/sqrat/include/

QMAKE_LFLAGS += -fPIC
DEFINES += SSH_LIBRARY
DEFINES += SO_LIBRARY

SOURCES += ssh.cpp

HEADERS += ssh.h\
        ssh_global.h

target.path = ../../bin/plugins
INSTALLS += target

unix|win32: LIBS += -lssh

