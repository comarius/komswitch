

#include <errno.h>
#include <iostream>
#include <string>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "ssh.h"

#include <libssh/libssh.h> //sudo apt-get install libssh-dev
#include <libssh/sftp.h>
#include <libssh/server.h>


int _Debug = 0;


template < typename T,class P>struct AutoCall
{
    AutoCall(T f,P p):_f(f),_p(p) {}
    ~AutoCall()
    {
        _f(_p);
    }
    T _f;
    P _p;
};

Ssh::Ssh():_ssh_session(0),_channel(0),_dev(0),_port(0)
{
}

Ssh::Ssh(IDevice* pdev,const char* cred,const char* name):_ssh_session(0),_channel(0),_dev(pdev),_cred(cred),_name(name),_port(0)
{

}

Ssh::~Ssh()
{
    close();
}

/**
 * @brief Ssh::open
 * @return
 */
int Ssh::open()
{
    std::string  s = _cred;
    std::string  a;
    int rc = 0;

    this->close();

    for(size_t ch = 0; s[ch]!=0; ch++)
    {
        if(s[ch]=='/')
        {
            _user=a;
            a.clear();
            continue;
        }
        if(s[ch]=='@')
        {
            _pass=a;
            a.clear();
            continue;
        }
        if(s[ch]==':')
        {
            if(_host.empty())
            {
                _host=a;
                a.clear();
                continue;
            }

            if(_port==0)
            {
                _port = ::atoi(a.c_str());
                a.clear();
                continue;
            }
        }
        a+=s[ch];
    }
    if(_port==0)
        _port=22;
    if(_host.empty() || _user.empty())
    {
        LOGERR ("Malformated connection string. No user and/or host");
        return -1;
    }

    int verbosity = _Debug;
    _ssh_session = ::ssh_new();
    if(0==_ssh_session)
    {
        std::cout << "ERROR ssh_new(): " << errno << std::endl;
        return errno;
    }
    ssh_options_set(_ssh_session,SSH_OPTIONS_HOST,_host.c_str());
    if(verbosity>4)
    {
        verbosity-=4;
        ssh_options_set(_ssh_session,SSH_OPTIONS_LOG_VERBOSITY,&verbosity);
    }
    ssh_options_set(_ssh_session,SSH_OPTIONS_PORT,&_port);
    ssh_options_set(_ssh_session,SSH_OPTIONS_USER,_user.c_str());
    rc = ssh_connect(_ssh_session);
    if (rc != SSH_OK)
    {
        LOGERR ("Connecting to " << _host << " " << ssh_get_error(_ssh_session));
        return -1;
    }
    rc = _verify_knownhost();
    if (rc != 0)
    {
        return rc;
    }
    if(!_pass.empty())
    {
        rc = ssh_userauth_password(_ssh_session,NULL,_pass.c_str());
        if (rc != SSH_AUTH_SUCCESS)
        {
            return rc;
        }
    }
    else
    {
        rc = ssh_userauth_password(_ssh_session,NULL,"");
        if (rc != SSH_AUTH_SUCCESS)
        {
            return rc;
        }
    }
    //
    // open tty
    //
    assert(_ssh_session);
    assert(_channel==0);

   _channel = channel_new(_ssh_session);
   if (_channel == NULL)
   {
       LOGERR("Error creating ssh channel"  << ssh_get_error(_ssh_session));
       return -1;
   }
   rc = channel_open_session(_channel);
   if (rc != SSH_OK)
   {
       LOGERR("Error openning ssh session"  << ssh_get_error(_ssh_session));
       return -1;
   }

   rc = channel_request_pty(_channel);
   if (rc != SSH_OK)
   {
       LOGERR("Error channel_request_pty "  << ssh_get_error(_ssh_session));
       return -1;
   }
   rc = channel_change_pty_size(_channel,80,24);
   if (rc != SSH_OK)
   {
       LOGERR("Error channel_change_pty_size 80x24 "  << ssh_get_error(_ssh_session));
       return -1;
   }
   rc = channel_request_shell(_channel);
   if (rc != SSH_OK)
   {
       LOGERR("Error channel_request_shell "  << ssh_get_error(_ssh_session));
       return -1;
   }

   if((channel_is_open(_channel) && !channel_is_eof(_channel)))
   {
       return 0;
   }
   return 1;
}

int Ssh::close()
{
    if(_channel)
    {
        channel_send_eof(_channel);
        sleep(1);
        channel_close(_channel);
        channel_free(_channel);
        _channel= 0;
    }

    if(_ssh_session)
    {
        ssh_disconnect(_ssh_session);
        ssh_free(_ssh_session);
        _ssh_session=0;
    }
    return 0;
}

/**
 * @brief Ssh::reopen
 * @param where <user/<password> >@device-location<:port[22]>
 * @return
 */
int Ssh::reopen(const char* where)
{
    _cred = where;
    close();
    ::sleep(1);
    return open();
}
void Ssh::setspeed(int ms)
{

}

int Ssh::sendTo(cbyte* bytes,int len)
{
    assert(_channel);
    size_t sent = 0;
    while(len > 0)
    {
        int nwritten = channel_write(_channel,bytes+sent,len);
        if(nwritten>0)
        {
            len-=nwritten;
            sent+=nwritten;
        }
        else
        {
            LOGERR(__FUNCTION__  << ssh_get_error(_ssh_session));
            break;
        }
    }
    return len;
}

int Ssh::recFrom(unsigned char* buffer,int room)
{
    assert(_channel);

    int nbytes = channel_read_nonblocking(_channel,buffer,room-1,0);
    if (nbytes <= 0)
    {
        const char* sslerr = ssh_get_error(_ssh_session);
        if(channel_is_eof(_channel))
        {
            LOGERR(__FUNCTION__  << ssh_get_error(_ssh_session));
            return -1;
        }
    }
    if (nbytes > 0){
        buffer[nbytes]= 0;
        LOGDETAIL2("Ssh received" <<  buffer << std::endl);
    }

    return nbytes;
}

void Ssh::debug(int d)
{
    _Debug = d;
    if(_Debug > 4)
    {
        int d = _Debug-4;
        if(_ssh_session)
            ::ssh_options_set(_ssh_session,SSH_OPTIONS_LOG_VERBOSITY,&d);
    }
}

void Ssh::setTout(int to, int tosel)
{
    if(_ssh_session)
        ::ssh_options_set(_ssh_session,SSH_OPTIONS_TIMEOUT,&to);
}

bool Ssh::isOpen()
{
    return (_channel && channel_is_open(_channel));// && !channel_is_eof(_channel));
}

void Ssh::flush()
{

}

int Ssh::esc(const char* dt,int how)
{
    return -1;
}

/**
 * @brief Ssh::setOptions
 *  OPTION=VALUE
 * @param cf
 * @return
 */
int Ssh::setOptions(const char* cf)
{
    int option = 0;
    int value = 0;

    if(2 == ::sscanf(cf,"%d=%d",&option,&value))
    {
        if(_ssh_session)
        {
            ::ssh_options_set(_ssh_session,(ssh_options_e)option,&value);
            return 0;
        }
        std::cout << __FUNCTION__ << " invalid option string: usage 'option_id=value'" << std::endl;
        return 1;
    }
    std::cout << __FUNCTION__ << " ssh session is closed" << std::endl;
    return -1;
}

/**
 * @brief Ssh::copyFile(/tmp/somfile.ext @)
 * @param fileS
 * @param fileD
 * @return
 */
int Ssh::copyFile(const char* fileS,const char* fileD,bool toremote)
{
    if(toremote)
        return _putFile(fileS,fileD);
    return _getFile(fileS,fileD);
}

int Ssh::removeFile()
{
    return -1;
}

int Ssh::createFile()
{
    return -1;
}

IDevice::D_MSG Ssh::getMode()const
{
    return IDevice::eTEXT;
}

const char* Ssh::name()const
{
    return _name.c_str();
}

int Ssh::_verify_knownhost()
{
    size_t  state=0,hlen=0;
    uint8_t *hash = 0;
    char     *hexa = 0;
    ssh_key  srv_pubkey = 0;

    state = ssh_is_server_known(_ssh_session);
    hlen = ssh_get_pubkey_hash(_ssh_session,&hash);
/*
this crashes when opening multiple ssh sessions
ssh_get_publickey_hash(srv_pubkey,
                        SSH_PUBLICKEY_HASH_SHA1,
                        &hash,
                        &hlen);
*/
    if ((int)hlen <= 0)
    {
        LOGERR("ssh_get_pubkey_hash: " << errno);
        return -1;
    }
    switch (state)
    {
    case SSH_SERVER_KNOWN_OK:
        break; /* ok */
    case SSH_SERVER_KNOWN_CHANGED:
        LOGDETAIL("Host key for server changed: it is now");
        LOGDETAIL("Public key hash" << hash << hlen);
        LOGDETAIL("For security reasons,connection will be stopped");
        free(hash);
        return -1;
    case SSH_SERVER_FOUND_OTHER:
        LOGDETAIL("The host key for this server was not found but an other"
                "type of key exists.");
        LOGDETAIL("An attacker might change the default server key to"
                "confuse your client into thinking the key does not exist");
        free(hash);
        return -1;
    case SSH_SERVER_FILE_NOT_FOUND:
        LOGDETAIL("Could not find known host file.");
        LOGDETAIL("If you accept the host key here,the file will be"
                "automatically created.");
    /* fallback to SSH_SERVER_NOT_KNOWN behavior */
    case SSH_SERVER_NOT_KNOWN:
        hexa = ssh_get_hexa(hash,hlen);
        LOGERR("The server is unknown. Do you trust the host key?");
        LOGERR( "Public key hash: " << hexa);
        free(hexa);
        /*
        if (fgets(buf,sizeof(buf),stdin) == NULL)
        {
            free(hash);
            return -1;
        }
        if (strncasecmp(buf,"yes",3) != 0)
        {
            free(hash);
            return -1;
        }
        */

        if (ssh_write_knownhost(_ssh_session) < 0)
        {
            LOGDETAIL("Error " << strerror(errno));
            free(hash);
            return -1;
        }
        break;
    case SSH_SERVER_ERROR:
        LOGDETAIL("Error" << ssh_get_error(_ssh_session));
        free(hash);
        return -1;
    }
    free(hash);
    return 0;
}


int Ssh::_putFile(const char* local,const char* remote)
{
    assert(_ssh_session);

    sftp_session sftp = sftp_new(_ssh_session);
    if(!sftp)
    {
        LOGERR("cannot open sftp_session" <<  ssh_get_error(_ssh_session));
        return -1;
    }
    AutoCall<void (*)(sftp_session),sftp_session> ac(::sftp_free,sftp);

    if(sftp_init(sftp))
    {
        LOGERR("error initialising sftp:" <<  ssh_get_error(_ssh_session));
        return -1;
    }
    sftp_file fremote = sftp_open(sftp,remote,O_WRONLY|O_CREAT,0644);

    if(!fremote)
    {
        fprintf(stderr,"Error opening %s: %s\n",remote ,ssh_get_error(_ssh_session));
        return -1;
    }

    AutoCall<int (*)(sftp_file),sftp_file>  acc(::sftp_close,fremote);

    FILE* pl = ::fopen(local,"rb");
    if(0==pl)
    {
        LOGERR("cannot open: " <<  local << "," << errno);
        return -1;
    }
    AutoCall<int (*) (FILE *),FILE*>    accc(::fclose,pl);
    uint8_t     buffer[4096];
    int         bytes;

    while(!feof(pl))
    {
        bytes = ::fread(buffer,1,sizeof(buffer),pl);
        if(bytes>0)
        {
             bytes=::sftp_write(fremote,buffer,bytes);
             _dev->devEvent(this,IDevice::eSENDING,buffer,bytes);
        }
        if(::feof(pl) || bytes <= 0)
            break;
        ::usleep(1000);
    }
    return 0;
}


int Ssh::_getFile(const char* remote,const char* local)
{
    assert(_ssh_session);
    sftp_session sftp = sftp_new(_ssh_session);
    if(!sftp)
    {
        LOGERR("cannot open sftp_session" <<  ssh_get_error(_ssh_session));
        return -1;
    }
    AutoCall<void (*)(sftp_session),sftp_session> ac(::sftp_free,sftp);

    if(sftp_init(sftp))
    {
        LOGERR("error initialising sftp:" <<  ssh_get_error(_ssh_session));
        return -1;
    }

    sftp_file fremote = sftp_open(sftp,remote,O_RDONLY,0);
    if(!fremote)
    {
        fprintf(stderr,"Error opening %s: %s\n",remote,ssh_get_error(_ssh_session));
        return -1;
    }

    AutoCall<int (*)(sftp_file),sftp_file>  acc(::sftp_close,fremote);

    FILE* pl = fopen(local,"wb");
    if(0==pl)
    {
        LOGERR("cannot open: " <<  local << "," << errno);
        return -1;
    }
    AutoCall<int (*) (FILE *),FILE*>    accc(fclose,pl);
    uint8_t                             buffer[4096];
    int                                 bytes,written;

    while((bytes=sftp_read(fremote,buffer,sizeof(buffer))) > 0)
    {
        written = ::fwrite(buffer,1,bytes,pl);
        if(bytes != written)
        {
             LOGERR("error writing to:" << local);
             return -1;
        }
        _dev->devEvent(this,IDevice::eRECEIVING,buffer,bytes);
    }
    return 0;
}

/**
 *
 */
extern "C"
{
    EXPORT_CLS  IComm* getInstance(IDevice* pd,const char* cs,const char* name)
    {
        return new Ssh(pd,cs,name);
    }

    EXPORT_CLS  void destroyInstance(IComm* pc)
    {
        delete pc;
    }
}


