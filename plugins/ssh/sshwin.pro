#-------------------------------------------------
#
# Project created by QtCreator 2017-10-12T13:12:29
#
#-------------------------------------------------

QT       -= core gui

TARGET = ssh
TEMPLATE = lib

DEFINES += SSH_LIBRARY

SOURCES += ssh.cpp

HEADERS += ssh.h\
        ssh_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
