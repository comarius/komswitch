
#include <stdlib.h>
#include <string.h>
#include "https.h"
#include "curl/curl.h"

Https::Https():_curl(0)
{

}

Https::Https(IDevice* pd, const char* cred):_pd(pd)
{
    char loco[512];

    ::strcpy(loco,cred);
    char* p = ::strtok(loco,",");
    if(p){
        _https = p;
        p = ::strtok(0,",");
        if(p){
            _cert = p;
            p = ::strtok(0,",");
            if(p){
                _userpass=p;
            }
        }
    }
}

Https::~Https()
{
    close();
}

// https://wwww/port
int Https::open()
{
    curl_global_init(CURL_GLOBAL_ALL);
    _curl = curl_easy_init();
    return _curl != 0 ? 0 : -1;
}

/**
 * @brief Https::close
 * @return
 * _cred = https://sadfasdfads./,user:password,cert:/path
 */
int Https::close()
{
    if(_curl)
    {
        curl_easy_cleanup(_curl);
        curl_global_cleanup();
         _curl = 0;
    }
}

int Https::sendTo(const uint8_t*, int)
{
    if(_curl)
    {
        curl_easy_setopt(_curl, CURLOPT_URL, _https.c_str());
        if(_https.find("https")!=std::string::npos)
        {
            if(!_cert.empty())
            {
                curl_easy_setopt(_curl, CURLOPT_CAPATH, _cert.c_str());
            }
            else
            {
                curl_easy_setopt(_curl, CURLOPT_SSL_VERIFYHOST, 0L);
                curl_easy_setopt(_curl, CURLOPT_SSL_VERIFYPEER, 0); //don't verify peer against cert
            }
        }

        curl_easy_setopt(_curl, CURLOPT_NOBODY, 1L);
        curl_easy_setopt( _curl, CURLOPT_USERAGENT, "AUTOCOM-1.0");
        curl_easy_setopt( _curl, CURLOPT_HEADER, 0 );
        curl_easy_setopt (_curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(_curl, CURLOPT_HEADERFUNCTION, &Https::writeCallbackHdr);
        curl_easy_setopt(_curl, CURLOPT_WRITEHEADER, this);
        curl_easy_setopt(_curl, CURLOPT_FILETIME, 1L);
        curl_easy_setopt(_curl, CURLOPT_NOSIGNAL, 1);       //disable signals to use with threads
        curl_easy_setopt(_curl, CURLOPT_FOLLOWLOCATION, 1); //bounce through login to next page
        int res = curl_easy_perform(_curl);
        if (CURLE_OK == res)
        {
            char *ct;
            curl_easy_getinfo(_curl, CURLINFO_CONTENT_TYPE, &ct);
        }
        return res;
    }
    return -1;
}

int Https::recFrom(unsigned char* buff, int bytes)
{
    if(_payload.empty())
        return 0;
    ::strncpy((char*)buff, _payload.c_str(), bytes);
    int pll=int(_payload.length());
    _payload.clear();
    return std::min(pll, bytes);
}

void Https::setTout(int to, int ta)
{
}

bool Https::isOpen()
{
    return _curl != 0;
}

int  Https::copyFile(const char* fileS, const char* fileD, bool)
{
    return -1;
}

const char* Https::name()const
{
    return _https.c_str();
}

void Https::setspeed(int)
{

}

void Https::flush()
{

}

int Https::esc(const char* c,int i)
{
    _data = curl_escape(c,i);
    return 0;
}

int Https::setOptions(const char* o)
{
    CURLoption co;
    return (int)curl_easy_setopt(_curl, co, o);
}

int Https::removeFile()
{
    return -1;
}

int Https::createFile()
{
    return -1;
}

int Https::reopen(const char* cred)
{
    char loco[512];

    ::strcpy(loco,cred);
    char* p = ::strtok(loco,",");
    if(p){
        _https = p;
        p = ::strtok(0,",");
        if(p){
            _cert = p;
            p = ::strtok(0,",");
            if(p){
                _userpass=p;
            }
        }
    }
    close();
    return open();
}


IDevice::D_MSG Https::getMode()const
{
    return IDevice::eTEXT;
}

IComm* getInstance(IDevice* pd, const char* cs)
{
    return new Https(pd, cs);
}


void destroyInstance(IComm* pc)
{
    delete pc;
}

size_t Https::writeCallbackHdr(char* buf, size_t size, size_t nmemb, void* up)
{
    Https* pthis = reinterpret_cast<Https*>(up);
    pthis->_payload.append(buf, size*nmemb);
    return size*nmemb; //tell curl how many bytes we handled
}

//-----------------------------------------------------------------------------
size_t Https::writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{
    Https* pthis = reinterpret_cast<Https*>(up);
    pthis->_payload.append(buf, size * nmemb);
    return size*nmemb; //tell curl how many bytes we handled
}
