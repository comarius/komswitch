#-------------------------------------------------
#
# Project created by QtCreator 2017-10-12T13:12:03
#
#-------------------------------------------------
QT       += core
QT       -= gui

TARGET = https
TEMPLATE = lib

DEFINES += HTTPS_LIBRARY
DEFINES += SO_LIBRARY

SOURCES += https.cpp

INCLUDEPATH  += $$PWD/../support/curl/include
INCLUDEPATH  += $$PWD/../../
INCLUDEPATH += ../../sq/include
INCLUDEPATH +=../../sq/sqrat/include/

HEADERS += https.h \
    httpsfactory.h

target.path = ../../bin/plugins
INSTALLS += target




win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../support/curl/mco-build/lib/release/ -lcurl
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../support/curl/mco-build/lib/debug/ -lcurl
else:unix: LIBS += -L$$PWD/../support/curl/mco-build/lib/ -lcurl

INCLUDEPATH += $$PWD/../support/curl/mco-build
DEPENDPATH += $$PWD/../support/curl/mco-build
